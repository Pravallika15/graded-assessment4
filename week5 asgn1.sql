Enter password: **************
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 31
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database Week1Assignment1
    -> ;
ERROR 1007 (HY000): Can't create database 'week1assignment1'; database exists
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| ems                |
| hcl                |
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| week1assignment1   |
| world              |
+--------------------+
9 rows in set (0.01 sec)

mysql> use week1assignment1;
Database changed
mysql> show tables;
+----------------------------+
| Tables_in_week1assignment1 |
+----------------------------+
| employee                   |
+----------------------------+
1 row in set (0.01 sec)

mysql>  CREATE TABLE employee(id INTEGER, name VARCHAR(10),department VARCHAR(10),salary FLOAT);
ERROR 1050 (42S01): Table 'employee' already exists
mysql> drop table employee;
Query OK, 0 rows affected (0.05 sec)

mysql>  CREATE TABLE employee(id INTEGER, name VARCHAR(10),department VARCHAR(10),salary FLOAT);
Query OK, 0 rows affected (0.05 sec)

mysql> show tables;
+----------------------------+
| Tables_in_week1assignment1 |
+----------------------------+
| employee                   |
+----------------------------+
1 row in set (0.00 sec)

mysql> INSERT INTO employee values(1,'Aman','IT',12000);
Query OK, 1 row affected (0.02 sec)

mysql> INSERT INTO employee values(2,'Bhuvan','HR',15000);
Query OK, 1 row affected (0.01 sec)

mysql> INSERT INTO employee values(3,'Chandan','Admin',18000);
Query OK, 1 row affected (0.01 sec)

mysql> select *from employee;
+------+---------+------------+--------+
| id   | name    | department | salary |
+------+---------+------------+--------+
|    1 | Aman    | IT         |  12000 |
|    2 | Bhuvan  | HR         |  15000 |
|    3 | Chandan | Admin      |  18000 |
+------+---------+------------+--------+
3 rows in set (0.00 sec)

mysql> select sum(salary)from employee;
+-------------+
| sum(salary) |
+-------------+
|       45000 |
+-------------+
1 row in set (0.00 sec)

mysql> select avg (salary) from employee;
+--------------+
| avg (salary) |
+--------------+
|        15000 |
+--------------+
1 row in set (0.00 sec)

mysql> select max(salary) from employee;
+-------------+
| max(salary) |
+-------------+
|       18000 |
+-------------+
1 row in set (0.00 sec)

mysql> select min(salary) from employee;
+-------------+
| min(salary) |
+-------------+
|       12000 |
+-------------+
1 row in set (0.00 sec)

mysql>  select SET salary =salary + (salary*.3) from employee;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'SET salary =salary + (salary*.3) from employee' at line 1
mysql> UPDATE employee SET salary=salary+(salary*30/100);
Query OK, 3 rows affected (0.01 sec)
Rows matched: 3  Changed: 3  Warnings: 0

mysql> select *from employee;
+------+---------+------------+--------+
| id   | name    | department | salary |
+------+---------+------------+--------+
|    1 | Aman    | IT         |  15600 |
|    2 | Bhuvan  | HR         |  19500 |
|    3 | Chandan | Admin      |  23400 |
+------+---------+------------+--------+
3 rows in set (0.00 sec)

mysql> truncate TABLE employee;
Query OK, 0 rows affected (0.10 sec)