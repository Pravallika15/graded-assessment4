Enter password: **************
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 13
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database MoviesDatabase;
Query OK, 1 row affected (0.01 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| ems                |
| hcl                |
| information_schema |
| moviedatabase      |
| movies             |
| moviesdatabase     |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| week1assignment1   |
| world              |
+--------------------+
12 rows in set (0.01 sec)

mysql> use MovieDatabase;
Database changed
mysql> show tables;
+-------------------------+
| Tables_in_moviedatabase |
+-------------------------+
| actor                   |
| director                |
| movies                  |
+-------------------------+
3 rows in set (0.01 sec)

mysql> CREATE TABLE ACTOR (
    -> ACT_ID INTEGER PRIMARY KEY,
    -> ACT_NAME VARCHAR(20),
    -> ACT_GENDER CHAR(1));
ERROR 1050 (42S01): Table 'actor' already exists
mysql> CREATE TABLE ACTOR (
    -> ACT_ID INTEGER PRIMARY KEY,
    -> ACT_NAME VARCHAR(20),
    -> ACT_GENDER CHAR(1));
ERROR 1050 (42S01): Table 'actor' already exists
mysql> desc actor;
+------------+-------------+------+-----+---------+-------+
| Field      | Type        | Null | Key | Default | Extra |
+------------+-------------+------+-----+---------+-------+
| Act_id     | int         | NO   | PRI | NULL    |       |
| Act_Name   | varchar(30) | YES  |     | NULL    |       |
| Act_Gender | varchar(10) | YES  |     | NULL    |       |
+------------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)

mysql>
mysql> CREATE TABLE DIRECTOR(
    -> DIR_ID INTEGER PRIMARY KEY,
    -> DIR_NAME VARCHAR(20),
    -> DIR_PHONE INTEGER);
ERROR 1050 (42S01): Table 'director' already exists
mysql> desc director;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| Dir_id    | int         | NO   | PRI | NULL    |       |
| Dir_Name  | varchar(30) | YES  |     | NULL    |       |
| Dir_Phone | int         | YES  |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> CREATE TABLE MOVIES(
    -> MOV_ID INTEGER PRIMARY KEY,
    -> MOV_TITLE VARCHAR(25),
    -> MOV_YEAR INTEGER,
    -> MOV_LANG VARCHAR(15),
    -> DIR_ID INTEGER,
    -> FOREIGN KEY (DIR_ID) REFERENCES DIRECTOR(DIR_ID));
ERROR 1050 (42S01): Table 'movies' already exists
mysql> desc movies;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| MOV_ID    | int         | NO   | PRI | NULL    |       |
| MOV_TITLE | varchar(25) | YES  |     | NULL    |       |
| MOV_YEAR  | int         | YES  |     | NULL    |       |
| MOV_LANG  | varchar(15) | YES  |     | NULL    |       |
| DIR_ID    | int         | YES  | MUL | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
5 rows in set (0.00 sec)

mysql> CREATE TABLE MOVIE_CAST(
    -> ACT_ID INTEGER,
    -> MOV_ID INTEGER,
    -> ROLE VARCHAR(10),
    -> PRIMARY KEY (ACT_ID,MOV_ID),
    -> FOREIGN KEY (ACT_ID) REFERENCES ACTOR(ACT_ID),
    -> FOREIGN KEY (MOV_ID) REFERENCES MOVIES(MOV_ID));
Query OK, 0 rows affected (0.10 sec)

mysql> desc movie-cast;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '-cast' at line 1
mysql> desc movie_cast;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| ACT_ID | int         | NO   | PRI | NULL    |       |
| MOV_ID | int         | NO   | PRI | NULL    |       |
| ROLE   | varchar(10) | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> CREATE TABLE RATING(
    -> MOV_ID INTEGER PRIMARY KEY,
    -> REV_STARS VARCHAR(25),
    -> FOREIGN KEY (MOV_ID) REFERENCES MOVIES(MOV_ID));
Query OK, 0 rows affected (0.04 sec)

mysql> desc rating;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| MOV_ID    | int         | NO   | PRI | NULL    |       |
| REV_STARS | varchar(25) | YES  |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

mysql> INSERT INTO ACTOR VALUES (1,'Anisha','f');
Query OK, 1 row affected (0.02 sec)

mysql>
mysql> INSERT INTO ACTOR VALUES (2,'Pavan','m');
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO ACTOR VALUES (3,'Pradeep','m');
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO ACTOR VALUES (4,'George','m');
Query OK, 1 row affected (0.01 sec)

mysql> select*from actor;
+--------+----------+------------+
| Act_id | Act_Name | Act_Gender |
+--------+----------+------------+
|      1 | Anisha   | f          |
|      2 | Pavan    | m          |
|      3 | Pradeep  | m          |
|      4 | George   | m          |
+--------+----------+------------+
4 rows in set (0.00 sec)

mysql> INSERT INTO DIRECTOR VALUES (1,'Rohit', 1234567890);
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO DIRECTOR VALUES (2,'Akash', 1345678901);
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO DIRECTOR VALUES (3,'Christopher', 1456789012);
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO DIRECTOR VALUES (4,'Pamela', 1567890123);
Query OK, 1 row affected (0.01 sec)

mysql> select*from director;
+--------+-------------+------------+
| Dir_id | Dir_Name    | Dir_Phone  |
+--------+-------------+------------+
|      1 | Rohit       | 1234567890 |
|      2 | Akash       | 1345678901 |
|      3 | Christopher | 1456789012 |
|      4 | Pamela      | 1567890123 |
+--------+-------------+------------+
4 rows in set (0.00 sec)

mysql> INSERT INTO MOVIES VALUES (1,'ABCD', 2020, 'Hindi', 1);
Query OK, 1 row affected (0.03 sec)

mysql>
mysql> INSERT INTO MOVIES VALUES (2,'BCDA', 2019, 'Telugu', 1);
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO MOVIES VALUES (3,'CDAB', 2021, 'English', 2);
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO MOVIES VALUES (4,'DABC', 2018, 'Kannada', 3);
Query OK, 1 row affected (0.01 sec)

mysql> select*from movies;
+--------+-----------+----------+----------+--------+
| MOV_ID | MOV_TITLE | MOV_YEAR | MOV_LANG | DIR_ID |
+--------+-----------+----------+----------+--------+
|      1 | ABCD      |     2020 | Hindi    |      1 |
|      2 | BCDA      |     2019 | Telugu   |      1 |
|      3 | CDAB      |     2021 | English  |      2 |
|      4 | DABC      |     2018 | Kannada  |      3 |
+--------+-----------+----------+----------+--------+
4 rows in set (0.00 sec)

mysql> NSERT INTO MOVIE_CAST VALUES (1, 2, 'Actress');
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'NSERT INTO MOVIE_CAST VALUES (1, 2, 'Actress')' at line 1
mysql>
mysql> INSERT INTO MOVIE_CAST VALUES (1, 1, 'Actress');
Query OK, 1 row affected (0.02 sec)

mysql>
mysql> INSERT INTO MOVIE_CAST VALUES (3, 3, 'Actor');
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO MOVIE_CAST VALUES (3, 2, 'Villain');
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO MOVIE_CAST VALUES (4, 4, 'actor');
Query OK, 1 row affected (0.01 sec)

mysql> select*from movie_cast;
+--------+--------+---------+
| ACT_ID | MOV_ID | ROLE    |
+--------+--------+---------+
|      1 |      1 | Actress |
|      3 |      2 | Villain |
|      3 |      3 | Actor   |
|      4 |      4 | actor   |
+--------+--------+---------+
4 rows in set (0.00 sec)

mysql> INSERT INTO RATING VALUES (1,4);
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO RATING VALUES (2,2);
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO RATING VALUES (3, 5);
Query OK, 1 row affected (0.01 sec)

mysql>
mysql> INSERT INTO RATING VALUES (4, 4);
Query OK, 1 row affected (0.01 sec)

mysql> select*from rating;
+--------+-----------+
| MOV_ID | REV_STARS |
+--------+-----------+
|      1 | 4         |
|      2 | 2         |
|      3 | 5         |
|      4 | 4         |
+--------+-----------+
4 rows in set (0.00 sec)

mysql> SELECT MOV_TITLE
    -> FROM MOVIES
    -> WHERE DIR_ID = (SELECT DIR_ID
    -> FROM DIRECTOR
    -> WHERE DIR_NAME='Akash');
+-----------+
| MOV_TITLE |
+-----------+
| CDAB      |
+-----------+
1 row in set (0.00 sec)

mysql> SELECT ACT_NAME FROM ACTOR A JOIN MOVIE_CAST C ON A.ACT_ID=C.ACT_ID JOIN MOVIES M ON C.MOV_ID=M.MOV_ID WHERE M.MOV_YEAR NOT BETWEEN 2018 AND 2021;
Empty set (0.00 sec)

mysql> SELECT ACT_NAME FROM ACTOR A JOIN MOVIE_CAST C ON A.ACT_ID=C.ACT_ID JOIN MOVIES M ON C.MOV_ID=M.MOV_ID WHERE M.MOV_YEAR NOT BETWEEN 2021 AND 2018;
+----------+
| ACT_NAME |
+----------+
| Anisha   |
| Pradeep  |
| Pradeep  |
| George   |
+----------+
4 rows in set (0.00 sec)

mysql> SELECT MOV_TITLE,MAX(REV_STARS) FROM MOVIES INNER JOIN RATING USING (MOV_ID) GROUP BY MOV_TITLE HAVING MAX(REV_STARS)>0;
+-----------+----------------+
| MOV_TITLE | MAX(REV_STARS) |
+-----------+----------------+
| ABCD      | 4              |
| BCDA      | 2              |
| CDAB      | 5              |
| DABC      | 4              |
+-----------+----------------+
4 rows in set (0.00 sec)

mysql> UPDATE RATING SET REV_STARS=5 WHERE MOV_ID IN (SELECT MOV_ID FROM MOVIES WHERE DIR_ID IN (SELECT DIR_ID FROM DIRECTOR WHERE DIR_NAME='PAMELA'));
Query OK, 0 rows affected (0.00 sec)
Rows matched: 0  Changed: 0  Warnings: 0

mysql> select*from rating;
+--------+-----------+
| MOV_ID | REV_STARS |
+--------+-----------+
|      1 | 4         |
|      2 | 2         |
|      3 | 5         |
|      4 | 4         |
+--------+-----------+
4 rows in set (0.00 sec)

mysql>