Enter password: **************
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 21
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> CREATE database ems;
Query OK, 1 row affected (0.02 sec)

mysql> CREATE TABLE DEPT
    ->
    ->
    ->
    -> CREATE TABLE DEPT(dno integer(3) PRIMARY KEY,dname varchar(20) CHECK(dname=upper (dname)), loacation varchar(15)
    -> );
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'CREATE TABLE DEPT(dno integer(3) PRIMARY KEY,dname varchar(20) CHECK(dname=upper' at line 5
mysql> CREATE TABLE DEPT(dno integer(3) PRIMARY KEY,dname varchar(20) CHECK(dname=upper (dname)), loacation varchar(15));
ERROR 1046 (3D000): No database selected
mysql> use ems;
Database changed
mysql>
mysql> CREATE TABLE DEPT(dno integer(3) PRIMARY KEY,dname varchar(20) CHECK(dname=upper (dname)), loacation varchar(15));
Query OK, 0 rows affected, 1 warning (0.07 sec)

mysql> show tables;
+---------------+
| Tables_in_ems |
+---------------+
| dept          |
+---------------+
1 row in set (0.00 sec)

mysql> desc dept;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| dno       | int         | NO   | PRI | NULL    |       |
| dname     | varchar(20) | YES  |     | NULL    |       |
| loacation | varchar(15) | YES  |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> insert into dept values(10,'development','banglore');
Query OK, 1 row affected (0.18 sec)

mysql> select *from dept;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
+-----+-------------+-----------+
1 row in set (0.00 sec)

mysql> select 'hello' from dual;
+-------+
| hello |
+-------+
| hello |
+-------+
1 row in set (0.00 sec)

mysql> select current_date from dual;
+--------------+
| current_date |
+--------------+
| 2021-12-29   |
+--------------+
1 row in set (0.00 sec)

mysql> alter table dept modify(dname varchar(20
    ->
    -> );
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '(dname varchar(20

)' at line 1
mysql> alter table dept modify(dname varchar(20) constraint chk CHECK(dname=upper(dname)));
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '(dname varchar(20) constraint chk CHECK(dname=upper(dname)))' at line 1
mysql> alter table dept modify(dname varchar(20) constraint chk CHECK(dname=upper(dname)));
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '(dname varchar(20) constraint chk CHECK(dname=upper(dname)))' at line 1
mysql> system clr;
'clr' is not recognized as an internal or external command,
operable program or batch file.
mysql> system clear;
'clear' is not recognized as an internal or external command,
operable program or batch file.
mysql> system clear
'clear' is not recognized as an internal or external command,
operable program or batch file.
mysql> cls
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'cls' at line 1
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql>
mysql> desc dept;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| dno       | int         | NO   | PRI | NULL    |       |
| dname     | varchar(20) | YES  |     | NULL    |       |
| loacation | varchar(15) | YES  |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)

mysql> select*from dept;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
+-----+-------------+-----------+
1 row in set (0.00 sec)

mysql> insert into dept values(20,'testing','chennai'),
    -> (30,'reserach','hyderabad'),
    -> (40,'sales','mumbai'),
    -> (50,'travels','pune');
Query OK, 4 rows affected (0.01 sec)
Records: 4  Duplicates: 0  Warnings: 0

mysql> select*from dept;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql> insert into dept values(20,'testing','chennai'),
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 1
mysql> insert into dept values(20,'testing','chennai');
ERROR 1062 (23000): Duplicate entry '20' for key 'dept.PRIMARY'
mysql> insert into dept values(null,'qa','chennai');
ERROR 1048 (23000): Column 'dno' cannot be null
mysql> CREATE TABLE employee(eid integer(3) PRIMARY KEY,ename varchar(12),salary double(10,2),comm double(10,2), job varchar(15),
    -> mid integer(3),doj date,dno,dno integer(3) refernces dept(no));
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ',dno integer(3) refernces dept(no))' at line 2
mysql> CREATE TABLE employee(eid integer(3) PRIMARY KEY,ename varchar(12),salary double(10,2),comm double(10,2), job varchar(15),
    -> mid integer(3),doj date,dno integer(3) refernces dept(no));
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'refernces dept(no))' at line 2
mysql> CREATE TABLE employee(eid integer(3) PRIMARY KEY,ename varchar(12),salary double(10,2),comm double(10,2), job varchar(15),
    -> mid integer(3),doj date,dno integer(3) ,refernces dept(no));

ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'references dept(no))' at line 2
mysql> CREATE TABLE employee(eid integer(3) PRIMARY KEY,ename varchar(12),salary double(10,2),comm double(10,2), job varchar(15),
    -> mid integer(3),doj date, dno integer(3) references dept(no));
Query OK, 0 rows affected, 5 warnings (0.05 sec)

mysql> desc employee;
+--------+--------------+------+-----+---------+-------+
| Field  | Type         | Null | Key | Default | Extra |
+--------+--------------+------+-----+---------+-------+
| eid    | int          | NO   | PRI | NULL    |       |
| ename  | varchar(12)  | YES  |     | NULL    |       |
| salary | double(10,2) | YES  |     | NULL    |       |
| comm   | double(10,2) | YES  |     | NULL    |       |
| job    | varchar(15)  | YES  |     | NULL    |       |
| mid    | int          | YES  |     | NULL    |       |
| doj    | date         | YES  |     | NULL    |       |
| dno    | int          | YES  |     | NULL    |       |
+--------+--------------+------+-----+---------+-------+
8 rows in set (0.01 sec)

mysql> select *from dept;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql> insert into employee values(101,'LIKITH',90000,null,'president',null,'1998-06-11',10);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee values(102,'PAVAN',50000,null,'manager',101,'1999-06-14',10);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee values(103,'VAMSHI',35000,null,'developer',102,'2000-02-11',10);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee values(104,'KIRAN',32000,null,'developer',102,'2001-04-05',10);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee values(105,'SAI',43000,null,'manager',101,'2001-04-05',20);
Query OK, 1 row affected (0.02 sec)

mysql> insert into employee values(106,'TOM',25000,2000,'tester',105,'1998-04-05',20);
Query OK, 1 row affected (0.12 sec)

mysql> insert into employee values(107,'VICKY',65000,5000,'analyst',101,'1992-04-11',30);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee values(108,'SRAVAN',15000,3000,'salesman',101,current_date,40);
Query OK, 1 row affected (0.01 sec)

mysql> insert into employee values(109,'JERRY',5000,300,'clerk',null,current_date,null);
Query OK, 1 row affected (0.02 sec)

mysql> select*from employee;
+-----+--------+----------+---------+-----------+------+------------+------+
| eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+--------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
+-----+--------+----------+---------+-----------+------+------------+------+
9 rows in set (0.00 sec)