
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 36
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> use ems;
Database changed
mysql> select *from employee;
+-----+------------+-----------+---------+-----------+------+------------+------+
| eid | ename      | salary    | comm    | job       | mid  | doj        | dno  |
+-----+------------+-----------+---------+-----------+------+------------+------+
| 101 | LIKITH     | 100000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      |  50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     |  35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      |  32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        |  43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        |  25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      |  65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     |  15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |   5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika |  35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
+-----+------------+-----------+---------+-----------+------+------------+------+
10 rows in set (0.00 sec)

mysql> select *from dept;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql>  select salary from employee;
+-----------+
| salary    |
+-----------+
|   5000.00 |
|  15000.00 |
|  25000.00 |
|  32000.00 |
|  35000.00 |
|  35000.00 |
|  43000.00 |
|  50000.00 |
|  65000.00 |
| 100000.00 |
+-----------+
10 rows in set (0.00 sec)

mysql> ^C
mysql>  select salary from employee;
+-----------+
| salary    |
+-----------+
|   5000.00 |
|  15000.00 |
|  25000.00 |
|  32000.00 |
|  35000.00 |
|  35000.00 |
|  43000.00 |
|  50000.00 |
|  65000.00 |
| 100000.00 |
+-----------+
10 rows in set (0.00 sec)

mysql> CREATE INDEX  salary_index ON EMPLOYEE(salary);
ERROR 1061 (42000): Duplicate key name 'salary_index'
mysql>  desc salary_index;
ERROR 1146 (42S02): Table 'ems.salary_index' doesn't exist
mysql> ^C
mysql> select salary from employee;
+-----------+
| salary    |
+-----------+
|   5000.00 |
|  15000.00 |
|  25000.00 |
|  32000.00 |
|  35000.00 |
|  35000.00 |
|  43000.00 |
|  50000.00 |
|  65000.00 |
| 100000.00 |
+-----------+
10 rows in set (0.00 sec)

mysql> show indexes;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 1
mysql> show indexes from employee;
+----------+------------+--------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| Table    | Non_unique | Key_name     | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment | Visible | Expression |
+----------+------------+--------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| employee |          0 | PRIMARY      |            1 | eid         | A         |           9 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
| employee |          1 | salary_index |            1 | salary      | A         |           9 |     NULL |   NULL | YES  | BTREE      |         |               | YES     | NULL       |
| employee |          1 | job_sal      |            1 | job         | A         |           7 |     NULL |   NULL | YES  | BTREE      |         |               | YES     | NULL       |
| employee |          1 | job_sal      |            2 | salary      | A         |           9 |     NULL |   NULL | YES  | BTREE      |         |               | YES     | NULL       |
+----------+------------+--------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
4 rows in set (0.04 sec)

mysql>  select * from dept;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql> select salary from employee where job = 'manager' or job = 'developer';
+----------+
| salary   |
+----------+
| 32000.00 |
| 35000.00 |
| 35000.00 |
| 43000.00 |
| 50000.00 |
+----------+
5 rows in set (0.00 sec)

mysql> select * from dept where location in('chennai','pune');
ERROR 1054 (42S22): Unknown column 'location' in 'where clause'
mysql> ^C
mysql> ^C
mysql> select * from dept where location in('chennai','delhi');
ERROR 1054 (42S22): Unknown column 'location' in 'where clause'
mysql> rename column loacation TO location
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'column loacation TO location' at line 1
mysql> alter table dept;
Query OK, 0 rows affected (0.01 sec)

mysql> rename column loacation to location;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'column loacation to location' at line 1
mysql> RENAME COLUMN loacation to Location
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'COLUMN loacation to Location' at line 1
mysql> ALTER TABLE dept
    -> RENAME COLUMN loacation TO location;
Query OK, 0 rows affected (0.09 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select *from dept;
+-----+-------------+-----------+
| dno | dname       | location  |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql> select * from dept where location in('chennai','pune');
+-----+---------+----------+
| dno | dname   | location |
+-----+---------+----------+
|  20 | testing | chennai  |
|  50 | travels | pune     |
+-----+---------+----------+
2 rows in set (0.00 sec)

mysql> select * from dept where location in('chennai',''hyderabad');
    '> ;
    '> select * from dept where location in('chennai','hyderabad');
    '>  ;    '> '    -> ';    '>
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'hyderabad');
;
select * from dept where location in('chennai','hyderabad');
 ;  ' at line 1
    '>
    '> ;
    '> select * from dept where location in('chennai','hyderabad');
    '> ;
    '> '
    -> '
    '> ;
    '> ^C
mysql> select * from dept where location in('chennai','hyderabad');
+-----+----------+-----------+
| dno | dname    | location  |
+-----+----------+-----------+
|  20 | testing  | chennai   |
|  30 | reserach | hyderabad |
+-----+----------+-----------+
2 rows in set (0.00 sec)

mysql>  select * from dept;
+-----+-------------+-----------+
| dno | dname       | location  |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql> select*from employee;
+-----+------------+-----------+---------+-----------+------+------------+------+
| eid | ename      | salary    | comm    | job       | mid  | doj        | dno  |
+-----+------------+-----------+---------+-----------+------+------------+------+
| 101 | LIKITH     | 100000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      |  50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     |  35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      |  32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        |  43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        |  25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      |  65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     |  15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |   5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika |  35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
+-----+------------+-----------+---------+-----------+------+------------+------+
10 rows in set (0.00 sec)

mysql>  create index job_sal on employee(job,salary);
ERROR 1061 (42000): Duplicate key name 'job_sal'
mysql>  select  job , salary from employee;
+-----------+-----------+
| job       | salary    |
+-----------+-----------+
| analyst   |  65000.00 |
| clerk     |   5000.00 |
| developer |  32000.00 |
| developer |  35000.00 |
| developer |  35000.00 |
| manager   |  43000.00 |
| manager   |  50000.00 |
| president | 100000.00 |
| salesman  |  15000.00 |
| tester    |  25000.00 |
+-----------+-----------+
10 rows in set (0.00 sec)

mysql> SELECT ROWID  from employee;
ERROR 1054 (42S22): Unknown column 'ROWID' in 'field list'
mysql>  select * from  employee;
+-----+------------+-----------+---------+-----------+------+------------+------+
| eid | ename      | salary    | comm    | job       | mid  | doj        | dno  |
+-----+------------+-----------+---------+-----------+------+------------+------+
| 101 | LIKITH     | 100000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      |  50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     |  35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      |  32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        |  43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        |  25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      |  65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     |  15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |   5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika |  35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
+-----+------------+-----------+---------+-----------+------+------------+------+
10 rows in set (0.00 sec)

mysql> select * from employee limit 3;
+-----+--------+-----------+------+-----------+------+------------+------+
| eid | ename  | salary    | comm | job       | mid  | doj        | dno  |
+-----+--------+-----------+------+-----------+------+------------+------+
| 101 | LIKITH | 100000.00 | NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN  |  50000.00 | NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI |  35000.00 | NULL | developer |  102 | 2000-02-11 |   10 |
+-----+--------+-----------+------+-----------+------+------------+------+
3 rows in set (0.00 sec)

mysql>  select * from employee limit 3  offset 2;
+-----+--------+----------+---------+-----------+------+------------+------+
| eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+--------+----------+---------+-----------+------+------------+------+
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN  | 32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
+-----+--------+----------+---------+-----------+------+------------+------+
3 rows in set (0.00 sec)

mysql>  select * from employee limit 3  offset 3;
+-----+-------+----------+---------+-----------+------+------------+------+
| eid | ename | salary   | comm    | job       | mid  | doj        | dno  |
+-----+-------+----------+---------+-----------+------+------------+------+
| 104 | KIRAN | 32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI   | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM   | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
+-----+-------+----------+---------+-----------+------+------------+------+
3 rows in set (0.00 sec)

mysql> select * from employee limit 3  offset 6;
+-----+--------+----------+---------+----------+------+------------+------+
| eid | ename  | salary   | comm    | job      | mid  | doj        | dno  |
+-----+--------+----------+---------+----------+------+------------+------+
| 107 | VICKY  | 65000.00 | 5000.00 | analyst  |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman |  101 | 2021-12-29 |   40 |
| 109 | JERRY  |  5000.00 |  300.00 | clerk    | NULL | 2021-12-29 | NULL |
+-----+--------+----------+---------+----------+------+------------+------+
3 rows in set (0.00 sec)

mysql>  select * from employee;
+-----+------------+-----------+---------+-----------+------+------------+------+
| eid | ename      | salary    | comm    | job       | mid  | doj        | dno  |
+-----+------------+-----------+---------+-----------+------+------------+------+
| 101 | LIKITH     | 100000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      |  50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     |  35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      |  32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        |  43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        |  25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      |  65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     |  15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |   5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika |  35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
+-----+------------+-----------+---------+-----------+------+------------+------+
10 rows in set (0.00 sec)

mysql> select  'Mr.' | 'pavan' from dual;
+-----------------+
| 'Mr.' | 'pavan' |
+-----------------+
|               0 |
+-----------------+
1 row in set, 2 warnings (0.00 sec)

mysql> select  'Mr.' || 'pavan' from dual;
+------------------+
| 'Mr.' || 'pavan' |
+------------------+
|                0 |
+------------------+
1 row in set, 3 warnings (0.00 sec)

mysql> select   concat('Mr.','likith') from dual;
+------------------------+
| concat('Mr.','likith') |
+------------------------+
| Mr.likith              |
+------------------------+
1 row in set (0.00 sec)

mysql> select  concat('Mr. ',ename) from employee;
+----------------------+
| concat('Mr. ',ename) |
+----------------------+
| Mr. LIKITH           |
| Mr. PAVAN            |
| Mr. VAMSHI           |
| Mr. KIRAN            |
| Mr. SAI              |
| Mr. TOM              |
| Mr. VICKY            |
| Mr. SRAVAN           |
| Mr. JERRY            |
| Mr. pravallika       |
+----------------------+
10 rows in set (0.00 sec)

mysql> select  ename.char_length() from employee;
ERROR 1305 (42000): FUNCTION ename.char_length does not exist
mysql>  select  ename.char_length from employee;
ERROR 1054 (42S22): Unknown column 'ename.char_length' in 'field list'
mysql> select  ename.length() from employee;
ERROR 1305 (42000): FUNCTION ename.length does not exist
mysql> select  length(ename) from employee;
+---------------+
| length(ename) |
+---------------+
|             6 |
|             5 |
|             6 |
|             5 |
|             3 |
|             3 |
|             5 |
|             6 |
|             5 |
|            10 |
+---------------+
10 rows in set (0.00 sec)

mysql> select  length(ename) ,ename from employee;
+---------------+------------+
| length(ename) | ename      |
+---------------+------------+
|             6 | LIKITH     |
|             5 | PAVAN      |
|             6 | VAMSHI     |
|             5 | KIRAN      |
|             3 | SAI        |
|             3 | TOM        |
|             5 | VICKY      |
|             6 | SRAVAN     |
|             5 | JERRY      |
|            10 | pravallika |
+---------------+------------+
10 rows in set (0.00 sec)

mysql>  select  length(ename) as length ,ename from employee;
+--------+------------+
| length | ename      |
+--------+------------+
|      6 | LIKITH     |
|      5 | PAVAN      |
|      6 | VAMSHI     |
|      5 | KIRAN      |
|      3 | SAI        |
|      3 | TOM        |
|      5 | VICKY      |
|      6 | SRAVAN     |
|      5 | JERRY      |
|     10 | pravallika |
+--------+------------+
10 rows in set (0.00 sec)

mysql>  select  length(ename) as length ,ename from employee  order by length(ename);
+--------+------------+
| length | ename      |
+--------+------------+
|      3 | SAI        |
|      3 | TOM        |
|      5 | PAVAN      |
|      5 | KIRAN      |
|      5 | VICKY      |
|      5 | JERRY      |
|      6 | LIKITH     |
|      6 | VAMSHI     |
|      6 | SRAVAN     |
|     10 | pravallika |
+--------+------------+
10 rows in set (0.00 sec)

mysql> select  length(ename) as length ,ename , eid , salary ,job from employee  order by length(ename);
+--------+------------+-----+-----------+-----------+
| length | ename      | eid | salary    | job       |
+--------+------------+-----+-----------+-----------+
|      3 | SAI        | 105 |  43000.00 | manager   |
|      3 | TOM        | 106 |  25000.00 | tester    |
|      5 | PAVAN      | 102 |  50000.00 | manager   |
|      5 | KIRAN      | 104 |  32000.00 | developer |
|      5 | VICKY      | 107 |  65000.00 | analyst   |
|      5 | JERRY      | 109 |   5000.00 | clerk     |
|      6 | LIKITH     | 101 | 100000.00 | president |
|      6 | VAMSHI     | 103 |  35000.00 | developer |
|      6 | SRAVAN     | 108 |  15000.00 | salesman  |
|     10 | pravallika | 110 |  35000.00 | developer |
+--------+------------+-----+-----------+-----------+
10 rows in set (0.00 sec)

mysql> select  current_date from dual;
+--------------+
| current_date |
+--------------+
| 2021-12-31   |
+--------------+
1 row in set (0.00 sec)

mysql>  select  dayofmonth(current_date) from dual;
+--------------------------+
| dayofmonth(current_date) |
+--------------------------+
|                       31 |
+--------------------------+
1 row in set (0.00 sec)

mysql> select ename , doj from employee;
+------------+------------+
| ename      | doj        |
+------------+------------+
| LIKITH     | 1998-06-11 |
| PAVAN      | 1999-06-14 |
| VAMSHI     | 2000-02-11 |
| KIRAN      | 2001-04-05 |
| SAI        | 2001-04-05 |
| TOM        | 1998-04-05 |
| VICKY      | 1992-04-11 |
| SRAVAN     | 2021-12-29 |
| JERRY      | 2021-12-29 |
| pravallika | 2021-12-30 |
+------------+------------+
10 rows in set (0.00 sec)

mysql> select  dayofyear(doj) from employee;
+----------------+
| dayofyear(doj) |
+----------------+
|            162 |
|            165 |
|             42 |
|             95 |
|             95 |
|             95 |
|            102 |
|            363 |
|            363 |
|            364 |
+----------------+
10 rows in set (0.00 sec)

mysql> select ename, dayofyear(doj) from employee;
+------------+----------------+
| ename      | dayofyear(doj) |
+------------+----------------+
| LIKITH     |            162 |
| PAVAN      |            165 |
| VAMSHI     |             42 |
| KIRAN      |             95 |
| SAI        |             95 |
| TOM        |             95 |
| VICKY      |            102 |
| SRAVAN     |            363 |
| JERRY      |            363 |
| pravallika |            364 |
+------------+----------------+
10 rows in set (0.00 sec)

mysql>  select   ename, last_day(doj) from employee;
+------------+---------------+
| ename      | last_day(doj) |
+------------+---------------+
| LIKITH     | 1998-06-30    |
| PAVAN      | 1999-06-30    |
| VAMSHI     | 2000-02-29    |
| KIRAN      | 2001-04-30    |
| SAI        | 2001-04-30    |
| TOM        | 1998-04-30    |
| VICKY      | 1992-04-30    |
| SRAVAN     | 2021-12-31    |
| JERRY      | 2021-12-31    |
| pravallika | 2021-12-31    |
+------------+---------------+
10 rows in set (0.00 sec)

mysql>  select dayofweek(doj)  from employee;
+----------------+
| dayofweek(doj) |
+----------------+
|              5 |
|              2 |
|              6 |
|              5 |
|              5 |
|              1 |
|              7 |
|              4 |
|              4 |
|              5 |
+----------------+
10 rows in set (0.00 sec)

mysql>  select weekday(doj)  from employee;
+--------------+
| weekday(doj) |
+--------------+
|            3 |
|            0 |
|            4 |
|            3 |
|            3 |
|            6 |
|            5 |
|            2 |
|            2 |
|            3 |
+--------------+
10 rows in set (0.00 sec)

mysql> select  weekday('2021-12-26') from dual;
+-----------------------+
| weekday('2021-12-26') |
+-----------------------+
|                     6 |
+-----------------------+
1 row in set (0.00 sec)

mysql> select  weekday('2021-12-27') from dual;
+-----------------------+
| weekday('2021-12-27') |
+-----------------------+
|                     0 |
+-----------------------+
1 row in set (0.00 sec)

mysql> select weekofyear(current_date) from dual;
+--------------------------+
| weekofyear(current_date) |
+--------------------------+
|                       52 |
+--------------------------+
1 row in set (0.00 sec)

mysql>  select ename, weekday(doj) from employee;
+------------+--------------+
| ename      | weekday(doj) |
+------------+--------------+
| LIKITH     |            3 |
| PAVAN      |            0 |
| VAMSHI     |            4 |
| KIRAN      |            3 |
| SAI        |            3 |
| TOM        |            6 |
| VICKY      |            5 |
| SRAVAN     |            2 |
| JERRY      |            2 |
| pravallika |            3 |
+------------+--------------+
10 rows in set (0.00 sec)

mysql> ^C
mysql> select  ename ,
    ->  select  ename ,    -> CASE    ->  WHEN  weekday(doj) = 1 THEN 'MONDAY'    -> WHEN  weekday(doj) = 2 THEN  'TUESDAY'    -> ELSE    ->    weekday(doj)    -> END  as  weekday    -> from  employee;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'select  ename ,    -> CASE    ->  WHEN  weekday(doj) = 1 THEN 'MONDAY'    -> WHE' at line 2
mysql> select  ename ,
    -> CASE
    -> WHEN  weekday(doj)=1 THEN 'MONDAY'
    -> WHEN weekday(doj)=2 THEN 'TUESDAY'
    -> ELSE
    -> weekday(doj)
    -> END as weekday
    -> from employee;
+------------+---------+
| ename      | weekday |
+------------+---------+
| LIKITH     | 3       |
| PAVAN      | 0       |
| VAMSHI     | 4       |
| KIRAN      | 3       |
| SAI        | 3       |
| TOM        | 6       |
| VICKY      | 5       |
| SRAVAN     | TUESDAY |
| JERRY      | TUESDAY |
| pravallika | 3       |
+------------+---------+
10 rows in set (0.00 sec)

mysql> select  to_days(current_date) from dual;
+-----------------------+
| to_days(current_date) |
+-----------------------+
|                738520 |
+-----------------------+
1 row in set (0.00 sec)

mysql> select  to_days(current_date) from employee;
+-----------------------+
| to_days(current_date) |
+-----------------------+
|                738520 |
|                738520 |
|                738520 |
|                738520 |
|                738520 |
|                738520 |
|                738520 |
|                738520 |
|                738520 |
|                738520 |
+-----------------------+
10 rows in set (0.00 sec)

mysql> select  to_days(current_date) from dual;
+-----------------------+
| to_days(current_date) |
+-----------------------+
|                738520 |
+-----------------------+
1 row in set (0.00 sec)

mysql> select  to_days(current_date)  -  to_days('1990-06-19') from dual;
+-------------------------------------------------+
| to_days(current_date)  -  to_days('1990-06-19') |
+-------------------------------------------------+
|                                           11518 |
+-------------------------------------------------+
1 row in set (0.00 sec)

mysql> select  (to_days(current_date)  -  to_days('1990-06-19') ) /12 from dual;
+--------------------------------------------------------+
| (to_days(current_date)  -  to_days('1990-06-19') ) /12 |
+--------------------------------------------------------+
|                                               959.8333 |
+--------------------------------------------------------+
1 row in set (0.00 sec)

mysql>  select  ((to_days(current_date)  -  to_days('1990-06-19') ) /12 ) /365from dual;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'dual' at line 1
mysql> select  ((to_days(current_date)  -  to_days('1990-06-19') ) /12 ) /365 from dual;
+----------------------------------------------------------------+
| ((to_days(current_date)  -  to_days('1990-06-19') ) /12 ) /365 |
+----------------------------------------------------------------+
|                                                     2.62968037 |
+----------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select  ((to_days(current_date)  -  to_days('1990-06-19') ) /365 )  from dual;
+------------------------------------------------------------+
| ((to_days(current_date)  -  to_days('1990-06-19') ) /365 ) |
+------------------------------------------------------------+
|                                                    31.5562 |
+------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> select  ((to_days(current_date)  -  to_days('1990-06-19') ) /365 )  as age from dual;
+---------+
| age     |
+---------+
| 31.5562 |
+---------+
1 row in set (0.00 sec)



mysql>  DELIMITER //
mysql> CREATE PROCEDURE  PRO1()
    -> BEGIN
    -> select * from employee;
    -> update employee set salary = salary + 1000 where eid =101;
    ->  select eid,ename, salary from  employee where eid = 101;
    ->
    ->  END
    -> //
Query OK, 0 rows affected (0.03 sec)

mysql> call proc()
    -> //
ERROR 1305 (42000): PROCEDURE ems.proc does not exist
mysql> call pro1();
    -> //
+-----+------------+-----------+---------+-----------+------+------------+------+
| eid | ename      | salary    | comm    | job       | mid  | doj        | dno  |
+-----+------------+-----------+---------+-----------+------+------------+------+
| 101 | LIKITH     | 100000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      |  50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     |  35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      |  32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        |  43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        |  25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      |  65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     |  15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |   5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika |  35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
+-----+------------+-----------+---------+-----------+------+------------+------+
10 rows in set (0.01 sec)

+-----+--------+-----------+
| eid | ename  | salary    |
+-----+--------+-----------+
| 101 | LIKITH | 101000.00 |
+-----+--------+-----------+
1 row in set (0.11 sec)

Query OK, 0 rows affected (0.13 sec)