Enter password: **************
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 17
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database Travel;
Query OK, 1 row affected (0.02 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| employee           |
| ems                |
| hcl                |
| information_schema |
| moviedatabase      |
| movies             |
| moviesdatabase     |
| mysql              |
| performance_schema |
| restarunt          |
| sakila             |
| sys                |
| travel             |
| week1assignment1   |
| world              |
+--------------------+
15 rows in set (0.01 sec)

mysql> use travel;
Database changed
mysql> create table PASSENGER (Passenger_name varchar(20),   Category varchar(20),   Gender varchar(20),   Boarding_City varchar(20), Destination_City  varchar(20),  Distance int,  Bus_Type varchar(20));
Query OK, 0 rows affected (0.09 sec)

mysql> create table PRICE(Bus_Type   varchar(20), Distance int, Price int);
Query OK, 0 rows affected (0.06 sec)

mysql> insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
Query OK, 1 row affected (0.01 sec)
mysql> insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');
Query OK, 1 row affected (0.01 sec)

mysql> select * from passenger;
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Anmol          | Non-AC   | M      | Mumbai        | Hyderabad        |      700 | Sitting  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Udit           | Non-AC   | M      | Trivandrum    | panaji           |     1000 | Sleeper  |
| Ankur          | AC       | M      | Nagpur        | Hyderabad        |      500 | Sitting  |
| Hemant         | Non-AC   | M      | panaji        | Mumbai           |      700 | Sleeper  |
| Manish         | Non-AC   | M      | Hyderabad     | Bengaluru        |      500 | Sitting  |
| Piyush         | AC       | M      | Pune          | Nagpur           |      700 | Sitting  |
+----------------+----------+--------+---------------+------------------+----------+----------+
9 rows in set (0.00 sec)

mysql> insert into price values('Sleeper',350,770);
Query OK, 1 row affected (0.02 sec)

mysql> insert into price values('Sleeper',500,1100);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sleeper',600,1320);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sleeper',700,1540);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sleeper',1000,2200);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sleeper',1200,2640);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sleeper',350,434);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sitting',500,620);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sitting',500,620);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sitting',600,744);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sitting',700,868);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sitting',1000,1240);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sitting',1200,1488);
Query OK, 1 row affected (0.01 sec)

mysql> insert into price values('Sitting',1500,1860);
Query OK, 1 row affected (0.01 sec)

mysql> select * from price;
+----------+----------+-------+
| Bus_Type | Distance | Price |
+----------+----------+-------+
| Sleeper  |      350 |   770 |
| Sleeper  |      500 |  1100 |
| Sleeper  |      600 |  1320 |
| Sleeper  |      700 |  1540 |
| Sleeper  |     1000 |  2200 |
| Sleeper  |     1200 |  2640 |
| Sleeper  |      350 |   434 |
| Sitting  |      500 |   620 |
| Sitting  |      500 |   620 |
| Sitting  |      600 |   744 |
| Sitting  |      700 |   868 |
| Sitting  |     1000 |  1240 |
| Sitting  |     1200 |  1488 |
| Sitting  |     1500 |  1860 |
+----------+----------+-------+
14 rows in set (0.00 sec)

mysql> select count(case when (Gender) = 'M' THEN 1 END) Male,count(case when (Gender) = 'F' THEN 1 END) Female from passenger where Distance>=600;
+------+--------+
| Male | Female |
+------+--------+
|    4 |      2 |
+------+--------+
1 row in set (0.00 sec)

mysql> select  min(Price) as Min_Price  from price where Bus_Type='sleeper';
+-----------+
| Min_Price |
+-----------+
|       434 |
+-----------+
1 row in set (0.00 sec)

mysql> select Passenger_name from passenger
    ->  where Passenger_name like 'S%'
    -> order by Passenger_name asc;
+----------------+
| Passenger_name |
+----------------+
| Sejal          |
+----------------+
1 row in set (0.00 sec)

mysql>  select distinct passenger.Passenger_name,passenger.Boarding_City,passenger.Destination_City,passenger.Bus_Type,price.Price
    -> from passenger, price where passenger.Distance=price.Distance and passenger.Bus_Type=price.Bus_Type
    -> group by passenger.Passenger_name;
+----------------+---------------+------------------+----------+-------+
| Passenger_name | Boarding_City | Destination_City | Bus_Type | Price |
+----------------+---------------+------------------+----------+-------+
| Sejal          | Bengaluru     | Chennai          | Sleeper  |   770 |
| Pallavi        | panaji        | Bengaluru        | Sleeper  |  1320 |
| Hemant         | panaji        | Mumbai           | Sleeper  |  1540 |
| Udit           | Trivandrum    | panaji           | Sleeper  |  2200 |
| Manish         | Hyderabad     | Bengaluru        | Sitting  |   620 |
| Ankur          | Nagpur        | Hyderabad        | Sitting  |   620 |
| Piyush         | Pune          | Nagpur           | Sitting  |   868 |
| Anmol          | Mumbai        | Hyderabad        | Sitting  |   868 |
+----------------+---------------+------------------+----------+-------+
8 rows in set (0.00 sec)

mysql> select passenger.Passenger_name,price.Price from passenger inner join price on passenger.Distance = price.Distance where passenger.Bus_Type = 'Sitting' and passenger.Distance >= 1000;
Empty set (0.00 sec)

mysql> SELECT p1.Passenger_name, p1.Boarding_city, p1.Destination_city, p1.Bus_type, p2.Price FROM passenger p1, price p2 WHERE p1.Distance = 1000 and p1.Bus_type = 'Sitting' and p1.Distance = 1000 and p1.Bus_type = 'Sitting';
Empty set (0.00 sec)

mysql>  select passenger.Passenger_name, price.Bus_Type, passenger.Distance, price.Price from passenger left join price on passenger.Distance=price.Distance  where passenger.Passenger_name='pallavi' and passenger.Boarding_City='Bengaluru' and passenger.Destination_City='panaji';
Empty set (0.00 sec)

mysql> SELECT DISTINCT distance FROM passenger ORDER BY Distance desc;
+----------+
| distance |
+----------+
|     1500 |
|     1000 |
|      700 |
|      600 |
|      500 |
|      350 |
+----------+
6 rows in set (0.00 sec)

mysql> SELECT Passenger_name, Distance * 100.0/ (SELECT SUM(Distance) FROM passenger)FROM passenger GROUP BY Distance;
+----------------+---------------------------------------------------------+
| Passenger_name | Distance * 100.0/ (SELECT SUM(Distance) FROM passenger) |
+----------------+---------------------------------------------------------+
| Sejal          |                                                 5.34351 |
| Anmol          |                                                10.68702 |
| Pallavi        |                                                 9.16031 |
| Khusboo        |                                                22.90076 |
| Udit           |                                                15.26718 |
| Ankur          |                                                 7.63359 |
+----------------+---------------------------------------------------------+
6 rows in set (0.00 sec)

mysql> create view passenger_in_AcBus as SELECT * FROM passenger WHERE Category = 'AC';
Query OK, 0 rows affected (0.04 sec)

mysql> select * from passenger_in_AcBus;
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Ankur          | AC       | M      | Nagpur        | Hyderabad        |      500 | Sitting  |
| Piyush         | AC       | M      | Pune          | Nagpur           |      700 | Sitting  |
+----------------+----------+--------+---------------+------------------+----------+----------+
5 rows in set (0.00 sec)

mysql> create procedure getp()
    -> BEGIN
    -> select  count(*) AS Passenger_count
    -> from passenger
    ->  where passenger.Bus_Type='sleeper';
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 5
mysql> create procedure getp()
    -> BEGIN
    -> select  count(*) AS Passenger_count
    -> from passenger
    ->  where passenger.Bus_Type='sleeper'
    -> END
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'END' at line 6
mysql> delimiter //
mysql> create procedure getp()
    -> BEGIN
    -> select  count(*) AS Passenger_count
    -> from passenger
    ->  where passenger.Bus_Type='sleeper';
    -> END
    -> //
Query OK, 0 rows affected (0.04 sec)

mysql> callgetp();
    -> //
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'callgetp()' at line 1
mysql> call getp();
    -> //
+-----------------+
| Passenger_count |
+-----------------+
|               5 |
+-----------------+
1 row in set (0.01 sec)

Query OK, 0 rows affected (0.01 sec)

mysql> SELECT * FROM passenger LIMIT 5;
    -> select * from passenger limit 5 offset 5;
    -> //
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Anmol          | Non-AC   | M      | Mumbai        | Hyderabad        |      700 | Sitting  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Udit           | Non-AC   | M      | Trivandrum    | panaji           |     1000 | Sleeper  |
+----------------+----------+--------+---------------+------------------+----------+----------+
5 rows in set (0.00 sec)

+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Ankur          | AC       | M      | Nagpur        | Hyderabad        |      500 | Sitting  |
| Hemant         | Non-AC   | M      | panaji        | Mumbai           |      700 | Sleeper  |
| Manish         | Non-AC   | M      | Hyderabad     | Bengaluru        |      500 | Sitting  |
| Piyush         | AC       | M      | Pune          | Nagpur           |      700 | Sitting  |
+----------------+----------+--------+---------------+------------------+----------+----------+
4 rows in set (0.06 sec)

mysql>