Enter password: **************
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> use database employee
ERROR 1049 (42000): Unknown database 'database'
mysql> use ems;
Database changed
mysql>  select * from employee;
+-----+------------+-----------+---------+-----------+------+------------+------+
| eid | ename      | salary    | comm    | job       | mid  | doj        | dno  |
+-----+------------+-----------+---------+-----------+------+------------+------+
| 101 | LIKITH     | 101000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      |  50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     |  35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      |  32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        |  43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        |  25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      |  65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     |  15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |   5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika |  35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
+-----+------------+-----------+---------+-----------+------+------------+------+
10 rows in set (0.03 sec)

mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> update employee set salary =10000 where eid=101;
Query OK, 1 row affected (0.02 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> roll back;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'roll back' at line 1
mysql> rollback;
Query OK, 0 rows affected (0.01 sec)

mysql> select *from employee;
+-----+------------+-----------+---------+-----------+------+------------+------+
| eid | ename      | salary    | comm    | job       | mid  | doj        | dno  |
+-----+------------+-----------+---------+-----------+------+------------+------+
| 101 | LIKITH     | 101000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      |  50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     |  35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      |  32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        |  43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        |  25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      |  65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     |  15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |   5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika |  35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
+-----+------------+-----------+---------+-----------+------+------------+------+
10 rows in set (0.00 sec)

mysql>  start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql>  update employee set salary =7000 where eid=101;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from employee;
+-----+------------+----------+---------+-----------+------+------------+------+
| eid | ename      | salary   | comm    | job       | mid  | doj        | dno  |
+-----+------------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH     |  7000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      | 32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika | 35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
+-----+------------+----------+---------+-----------+------+------------+------+
10 rows in set (0.00 sec)

mysql>  commit;
Query OK, 0 rows affected (0.01 sec)

mysql> start transaction;
Query OK, 0 rows affected (0.00 sec)

mysql> insert into employee(eid,ename,salary,job,mid,doj,dno) values(110,'venkat',32000,'developer',102,current_date,10);
ERROR 1062 (23000): Duplicate entry '110' for key 'employee.PRIMARY'
mysql> insert into employee(eid,ename,salary,job,mid,doj,dno) values(111,'venkat',32000,'developer',102,current_date,10);
Query OK, 1 row affected (0.00 sec)

mysql> update employee set comm=1000 where eid=104;
Query OK, 0 rows affected (0.00 sec)
Rows matched: 1  Changed: 0  Warnings: 0

mysql> savepoint s1;
Query OK, 0 rows affected (0.00 sec)

mysql> delete from employee where eid=108;
Query OK, 1 row affected (0.00 sec)

mysql>  select * from employee;
+-----+------------+----------+---------+-----------+------+------------+------+
| eid | ename      | salary   | comm    | job       | mid  | doj        | dno  |
+-----+------------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH     |  7000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      | 32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 109 | JERRY      |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika | 35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
| 111 | venkat     | 32000.00 |    NULL | developer |  102 | 2022-01-03 |   10 |
+-----+------------+----------+---------+-----------+------+------------+------+
10 rows in set (0.00 sec)

mysql>  rollback to s1;
Query OK, 0 rows affected (0.00 sec)

mysql>  select * from employee;
+-----+------------+----------+---------+-----------+------+------------+------+
| eid | ename      | salary   | comm    | job       | mid  | doj        | dno  |
+-----+------------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH     |  7000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      | 32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika | 35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
| 111 | venkat     | 32000.00 |    NULL | developer |  102 | 2022-01-03 |   10 |
+-----+------------+----------+---------+-----------+------+------------+------+
11 rows in set (0.00 sec)

mysql>  commit;
Query OK, 0 rows affected (0.01 sec)

mysql>  select * from employee;
+-----+------------+----------+---------+-----------+------+------------+------+
| eid | ename      | salary   | comm    | job       | mid  | doj        | dno  |
+-----+------------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH     |  7000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN      | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI     | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN      | 32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI        | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM        | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY      | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN     | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY      |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 110 | pravallika | 35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
| 111 | venkat     | 32000.00 |    NULL | developer |  102 | 2022-01-03 |   10 |
+-----+------------+----------+---------+-----------+------+------------+------+
11 rows in set (0.00 sec)

mysql> select*from dept;
+-----+-------------+-----------+
| dno | dname       | location  |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.04 sec)

mysql> select eid,ename,job,location, from emloyee e , dept d  where e.dno = d.dno and d.dname IN ('development','testing');
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'from emloyee e , dept d  where e.dno = d.dno and d.dname IN ('development','test' at line 1
mysql> select eid,ename,job,location  from employee e , dept d  where e.dno = d.dno and d.dname IN('develoment','testing');
Empty set (0.01 sec)

mysql> select eid,ename,job,location  from employee e , dept d  where e.dno = d.dno and d.dname IN('develoment','testing');
Empty set (0.00 sec)

mysql> select eid,ename,job,location  from employee e , dept d  where e.dno = d.dno and d.dname IN(select dno from dept where d.dname IN('develoment','testing'));
Empty set (0.00 sec)

mysql> select eid,ename,job,location  from employee e , dept d  where e.dno = d.dno IN(select dno from dept where d.dname IN('develoment','testing'));
Empty set (0.00 sec)

mysql> select  projection     from tables        where        groupby            having         orderby;
ERROR 1146 (42S02): Table 'ems.tables' doesn't exist
mysql> select (select sum(salary) from employee ) from employee;
+-------------------------------------+
| (select sum(salary) from employee ) |
+-------------------------------------+
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
|                           344000.00 |
+-------------------------------------+
11 rows in set (0.01 sec)

mysql> select (select sum(salary) from employee ) total  from employee;
+-----------+
| total     |
+-----------+
| 344000.00 |
| 344000.00 |
| 344000.00 |
| 344000.00 |
| 344000.00 |
| 344000.00 |
| 344000.00 |
| 344000.00 |
| 344000.00 |
| 344000.00 |
| 344000.00 |
+-----------+
11 rows in set (0.00 sec)

mysql> select ename,salary, (select sum(salary) from employee )-salary budget  from employee;
+------------+----------+-----------+
| ename      | salary   | budget    |
+------------+----------+-----------+
| LIKITH     |  7000.00 | 337000.00 |
| PAVAN      | 50000.00 | 294000.00 |
| VAMSHI     | 35000.00 | 309000.00 |
| KIRAN      | 32000.00 | 312000.00 |
| SAI        | 43000.00 | 301000.00 |
| TOM        | 25000.00 | 319000.00 |
| VICKY      | 65000.00 | 279000.00 |
| SRAVAN     | 15000.00 | 329000.00 |
| JERRY      |  5000.00 | 339000.00 |
| pravallika | 35000.00 | 309000.00 |
| venkat     | 32000.00 | 312000.00 |
+------------+----------+-----------+
11 rows in set (0.00 sec)

mysql> select eid,ename  from (select eid,ename, salary  from employee) emp;
+-----+------------+
| eid | ename      |
+-----+------------+
| 101 | LIKITH     |
| 102 | PAVAN      |
| 103 | VAMSHI     |
| 104 | KIRAN      |
| 105 | SAI        |
| 106 | TOM        |
| 107 | VICKY      |
| 108 | SRAVAN     |
| 109 | JERRY      |
| 110 | pravallika |
| 111 | venkat     |
+-----+------------+
11 rows in set (0.00 sec)

mysql> select eid,ename,job  from (select eid,ename, salary,job  from employee) emp;
+-----+------------+-----------+
| eid | ename      | job       |
+-----+------------+-----------+
| 101 | LIKITH     | president |
| 102 | PAVAN      | manager   |
| 103 | VAMSHI     | developer |
| 104 | KIRAN      | developer |
| 105 | SAI        | manager   |
| 106 | TOM        | tester    |
| 107 | VICKY      | analyst   |
| 108 | SRAVAN     | salesman  |
| 109 | JERRY      | clerk     |
| 110 | pravallika | developer |
| 111 | venkat     | developer |
+-----+------------+-----------+
11 rows in set (0.00 sec)

mysql> select eid,ename,salary,job from (select * from employee where job='developer')  emp;
+-----+------------+----------+-----------+
| eid | ename      | salary   | job       |
+-----+------------+----------+-----------+
| 104 | KIRAN      | 32000.00 | developer |
| 111 | venkat     | 32000.00 | developer |
| 103 | VAMSHI     | 35000.00 | developer |
| 110 | pravallika | 35000.00 | developer |
+-----+------------+----------+-----------+
4 rows in set (0.00 sec)

mysql> select * from dept;
+-----+-------------+-----------+
| dno | dname       | location  |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql>  create table dept2 as(select *from dept);
Query OK, 5 rows affected (0.37 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> desc dept2;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| dno      | int         | NO   |     | NULL    |       |
| dname    | varchar(20) | YES  |     | NULL    |       |
| location | varchar(15) | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+
3 rows in set (0.04 sec)

mysql> select * from dept2;
+-----+-------------+-----------+
| dno | dname       | location  |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql> create table dept3 as(select *from dept where 1=2);
Query OK, 0 rows affected (0.05 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> decc dept3;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'decc dept3' at line 1
mysql> desc dept3;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| dno      | int         | NO   |     | NULL    |       |
| dname    | varchar(20) | YES  |     | NULL    |       |
| location | varchar(15) | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> select * from dept3;
Empty set (0.00 sec)

mysql> select * from employee limit 2;
+-----+--------+----------+------+-----------+------+------------+------+
| eid | ename  | salary   | comm | job       | mid  | doj        | dno  |
+-----+--------+----------+------+-----------+------+------------+------+
| 101 | LIKITH |  7000.00 | NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN  | 50000.00 | NULL | manager   |  101 | 1999-06-14 |   10 |
+-----+--------+----------+------+-----------+------+------------+------+
2 rows in set (0.00 sec)

mysql> select * from employee  order by salary ;
+-----+------------+----------+---------+-----------+------+------------+------+
| eid | ename      | salary   | comm    | job       | mid  | doj        | dno  |
+-----+------------+----------+---------+-----------+------+------------+------+
| 109 | JERRY      |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 101 | LIKITH     |  7000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 108 | SRAVAN     | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 106 | TOM        | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 104 | KIRAN      | 32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 111 | venkat     | 32000.00 |    NULL | developer |  102 | 2022-01-03 |   10 |
| 103 | VAMSHI     | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 110 | pravallika | 35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
| 105 | SAI        | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 102 | PAVAN      | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 107 | VICKY      | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
+-----+------------+----------+---------+-----------+------+------------+------+
11 rows in set (0.00 sec)

mysql> select * from employee  order by salary ;
+-----+------------+----------+---------+-----------+------+------------+------+
| eid | ename      | salary   | comm    | job       | mid  | doj        | dno  |
+-----+------------+----------+---------+-----------+------+------------+------+
| 109 | JERRY      |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 101 | LIKITH     |  7000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 108 | SRAVAN     | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 106 | TOM        | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 104 | KIRAN      | 32000.00 | 1000.00 | developer |  102 | 2001-04-05 |   10 |
| 111 | venkat     | 32000.00 |    NULL | developer |  102 | 2022-01-03 |   10 |
| 103 | VAMSHI     | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 110 | pravallika | 35000.00 |    NULL | developer |  102 | 2021-12-30 |   10 |
| 105 | SAI        | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 102 | PAVAN      | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 107 | VICKY      | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
+-----+------------+----------+---------+-----------+------+------------+------+
11 rows in set (0.00 sec)

mysql>  select * from employee  order by salary limit 2;
+-----+--------+---------+--------+-----------+------+------------+------+
| eid | ename  | salary  | comm   | job       | mid  | doj        | dno  |
+-----+--------+---------+--------+-----------+------+------------+------+
| 109 | JERRY  | 5000.00 | 300.00 | clerk     | NULL | 2021-12-29 | NULL |
| 101 | LIKITH | 7000.00 |   NULL | president | NULL | 1998-06-11 |   10 |
+-----+--------+---------+--------+-----------+------+------------+------+
2 rows in set (0.00 sec)

mysql> select * from employee  order by salary desc limit 2;
+-----+-------+----------+---------+---------+------+------------+------+
| eid | ename | salary   | comm    | job     | mid  | doj        | dno  |
+-----+-------+----------+---------+---------+------+------------+------+
| 107 | VICKY | 65000.00 | 5000.00 | analyst |  101 | 1992-04-11 |   30 |
| 102 | PAVAN | 50000.00 |    NULL | manager |  101 | 1999-06-14 |   10 |
+-----+-------+----------+---------+---------+------+------------+------+
2 rows in set (0.00 sec)

mysql>  select * from employee  order by salary desc limit 1;
+-----+-------+----------+---------+---------+------+------------+------+
| eid | ename | salary   | comm    | job     | mid  | doj        | dno  |
+-----+-------+----------+---------+---------+------+------------+------+
| 107 | VICKY | 65000.00 | 5000.00 | analyst |  101 | 1992-04-11 |   30 |
+-----+-------+----------+---------+---------+------+------------+------+
1 row in set (0.00 sec)

mysql> select max(salary) from employee where salary <(select max(salary) from employee);
+-------------+
| max(salary) |
+-------------+
|    50000.00 |
+-------------+
1 row in set (0.00 sec)

mysql> select min(salary) from employee where salary >(select min(salary) from employee);
+-------------+
| min(salary) |
+-------------+
|     7000.00 |
+-------------+
1 row in set (0.00 sec)

mysql>