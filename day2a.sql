
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 27
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> desc dept;
ERROR 1046 (3D000): No database selected
mysql> use ems;
Database changed
mysql> desc dept;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| dno       | int         | NO   | PRI | NULL    |       |
| dname     | varchar(20) | YES  |     | NULL    |       |
| loacation | varchar(15) | YES  |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

mysql> insert into dept values(10,'development','banglore');
ERROR 1062 (23000): Duplicate entry '10' for key 'dept.PRIMARY'
mysql> select*from depr;
ERROR 1146 (42S02): Table 'ems.depr' doesn't exist
mysql> select*from dept;;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

ERROR:
No query specified

mysql> select*from dept;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql> CREATE TABLE employee(eid integer(3) PRIMARY KEY,ename VARCHAR(12),salary DOUBLE(10,2), comm DOUBLE(10,2), job VARCHAR(15), mid INTEGER(3), doj date ,dno INTEGER(3) references  dept(dno));
ERROR 1050 (42S01): Table 'employee' already exists
mysql> desc employee;
+--------+--------------+------+-----+---------+-------+
| Field  | Type         | Null | Key | Default | Extra |
+--------+--------------+------+-----+---------+-------+
| eid    | int          | NO   | PRI | NULL    |       |
| ename  | varchar(12)  | YES  |     | NULL    |       |
| salary | double(10,2) | YES  |     | NULL    |       |
| comm   | double(10,2) | YES  |     | NULL    |       |
| job    | varchar(15)  | YES  |     | NULL    |       |
| mid    | int          | YES  |     | NULL    |       |
| doj    | date         | YES  |     | NULL    |       |
| dno    | int          | YES  |     | NULL    |       |
+--------+--------------+------+-----+---------+-------+
8 rows in set (0.00 sec)

mysql> select*from dept;
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
|  20 | testing     | chennai   |
|  30 | reserach    | hyderabad |
|  40 | sales       | mumbai    |
|  50 | travels     | pune      |
+-----+-------------+-----------+
5 rows in set (0.00 sec)

mysql> insert into employee values(101,'KING',99000,null,'president',null,'1990-06-19',10);
ERROR 1062 (23000): Duplicate entry '101' for key 'employee.PRIMARY'
mysql> select *from employee;
+-----+--------+----------+---------+-----------+------+------------+------+
| eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+--------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
+-----+--------+----------+---------+-----------+------+------------+------+
9 rows in set (0.00 sec)

mysql> select * from employee,dept;
+-----+--------+----------+---------+-----------+------+------------+------+-----+-------------+-----------+
| eid | ename  | salary   | comm    | job       | mid  | doj        | dno  | dno | dname       | loacation |
+-----+--------+----------+---------+-----------+------+------------+------+-----+-------------+-----------+
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |  50 | travels     | pune      |
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |  40 | sales       | mumbai    |
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |  30 | reserach    | hyderabad |
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |  20 | testing     | chennai   |
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |  10 | development | banglore  |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |  50 | travels     | pune      |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |  40 | sales       | mumbai    |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |  30 | reserach    | hyderabad |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |  20 | testing     | chennai   |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |  10 | development | banglore  |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |  50 | travels     | pune      |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |  40 | sales       | mumbai    |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |  30 | reserach    | hyderabad |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |  20 | testing     | chennai   |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |  10 | development | banglore  |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |  50 | travels     | pune      |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |  40 | sales       | mumbai    |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |  30 | reserach    | hyderabad |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |  20 | testing     | chennai   |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |  10 | development | banglore  |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |  50 | travels     | pune      |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |  40 | sales       | mumbai    |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |  30 | reserach    | hyderabad |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |  20 | testing     | chennai   |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |  10 | development | banglore  |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |  50 | travels     | pune      |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |  40 | sales       | mumbai    |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |  30 | reserach    | hyderabad |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |  20 | testing     | chennai   |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |  10 | development | banglore  |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |  50 | travels     | pune      |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |  40 | sales       | mumbai    |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |  30 | reserach    | hyderabad |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |  20 | testing     | chennai   |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |  10 | development | banglore  |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |  50 | travels     | pune      |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |  40 | sales       | mumbai    |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |  30 | reserach    | hyderabad |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |  20 | testing     | chennai   |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |  10 | development | banglore  |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |  50 | travels     | pune      |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |  40 | sales       | mumbai    |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |  30 | reserach    | hyderabad |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |  20 | testing     | chennai   |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |  10 | development | banglore  |
+-----+--------+----------+---------+-----------+------+------------+------+-----+-------------+-----------+
45 rows in set (0.00 sec)

mysql>  select * from dept, employee;
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
| dno | dname       | loacation | eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
|  50 | travels     | pune      | 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
|  40 | sales       | mumbai    | 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
|  30 | reserach    | hyderabad | 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
|  20 | testing     | chennai   | 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
|  10 | development | banglore  | 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
|  50 | travels     | pune      | 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
|  40 | sales       | mumbai    | 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
|  30 | reserach    | hyderabad | 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
|  20 | testing     | chennai   | 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
|  10 | development | banglore  | 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
|  50 | travels     | pune      | 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
|  40 | sales       | mumbai    | 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
|  30 | reserach    | hyderabad | 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
|  20 | testing     | chennai   | 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
|  10 | development | banglore  | 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
|  50 | travels     | pune      | 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
|  40 | sales       | mumbai    | 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
|  30 | reserach    | hyderabad | 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
|  20 | testing     | chennai   | 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
|  10 | development | banglore  | 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
|  50 | travels     | pune      | 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
|  40 | sales       | mumbai    | 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
|  30 | reserach    | hyderabad | 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
|  20 | testing     | chennai   | 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
|  10 | development | banglore  | 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
|  50 | travels     | pune      | 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
|  40 | sales       | mumbai    | 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
|  30 | reserach    | hyderabad | 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
|  20 | testing     | chennai   | 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
|  10 | development | banglore  | 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
|  50 | travels     | pune      | 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
|  40 | sales       | mumbai    | 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
|  30 | reserach    | hyderabad | 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
|  20 | testing     | chennai   | 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
|  10 | development | banglore  | 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
|  50 | travels     | pune      | 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
|  40 | sales       | mumbai    | 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
|  30 | reserach    | hyderabad | 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
|  20 | testing     | chennai   | 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
|  10 | development | banglore  | 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
|  50 | travels     | pune      | 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
|  40 | sales       | mumbai    | 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
|  30 | reserach    | hyderabad | 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
|  20 | testing     | chennai   | 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
|  10 | development | banglore  | 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
45 rows in set (0.00 sec)

mysql>  select * from dept, employee where employee.dno=dept.dno;
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
| dno | dname       | loacation | eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
|  10 | development | banglore  | 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
|  10 | development | banglore  | 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
|  10 | development | banglore  | 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
|  10 | development | banglore  | 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
|  10 | development | banglore  | 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
|  10 | development | banglore  | 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
|  30 | reserach    | hyderabad | 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
|  40 | sales       | mumbai    | 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
8 rows in set (0.00 sec)

mysql> select * from dept d, employee e  where e.dno=d.dno;
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
| dno | dname       | loacation | eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
|  10 | development | banglore  | 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
|  10 | development | banglore  | 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
|  10 | development | banglore  | 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
|  10 | development | banglore  | 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
|  10 | development | banglore  | 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
|  10 | development | banglore  | 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
|  30 | reserach    | hyderabad | 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
|  40 | sales       | mumbai    | 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
+-----+-------------+-----------+-----+--------+----------+---------+-----------+------+------------+------+
8 rows in set (0.00 sec)

mysql> select  d.dno ,dname,eid,ename from dept d, employee e  where e.dno=d.dno;;
+-----+-------------+-----+--------+
| dno | dname       | eid | ename  |
+-----+-------------+-----+--------+
|  10 | development | 101 | LIKITH |
|  10 | development | 102 | PAVAN  |
|  10 | development | 103 | VAMSHI |
|  10 | development | 104 | KIRAN  |
|  10 | development | 105 | SAI    |
|  10 | development | 106 | TOM    |
|  30 | reserach    | 107 | VICKY  |
|  40 | sales       | 108 | SRAVAN |
+-----+-------------+-----+--------+
8 rows in set (0.00 sec)

ERROR:
No query specified

mysql> select  d.dno ,dname,eid,ename from dept d  JOIN employee e  where e.dno=d.dno;;
+-----+-------------+-----+--------+
| dno | dname       | eid | ename  |
+-----+-------------+-----+--------+
|  10 | development | 101 | LIKITH |
|  10 | development | 102 | PAVAN  |
|  10 | development | 103 | VAMSHI |
|  10 | development | 104 | KIRAN  |
|  10 | development | 105 | SAI    |
|  10 | development | 106 | TOM    |
|  30 | reserach    | 107 | VICKY  |
|  40 | sales       | 108 | SRAVAN |
+-----+-------------+-----+--------+
8 rows in set (0.00 sec)

ERROR:
No query specified

mysql> select  d.dno ,dname,eid,ename from dept d INNEER JOIN employee e  where e.dno=d.dno;;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'INNEER JOIN employee e  where e.dno=d.dno' at line 1
ERROR:
No query specified

mysql> select  d.dno ,dname,eid,ename from dept d  JOIN employee e  where e.dno=d.dno;;
+-----+-------------+-----+--------+
| dno | dname       | eid | ename  |
+-----+-------------+-----+--------+
|  10 | development | 101 | LIKITH |
|  10 | development | 102 | PAVAN  |
|  10 | development | 103 | VAMSHI |
|  10 | development | 104 | KIRAN  |
|  10 | development | 105 | SAI    |
|  10 | development | 106 | TOM    |
|  30 | reserach    | 107 | VICKY  |
|  40 | sales       | 108 | SRAVAN |
+-----+-------------+-----+--------+
8 rows in set (0.00 sec)

ERROR:
No query specified

mysql> select  d.dno ,dname,eid,ename from dept d INNER  JOIN employee e  where e.dno=d.dno;
+-----+-------------+-----+--------+
| dno | dname       | eid | ename  |
+-----+-------------+-----+--------+
|  10 | development | 101 | LIKITH |
|  10 | development | 102 | PAVAN  |
|  10 | development | 103 | VAMSHI |
|  10 | development | 104 | KIRAN  |
|  10 | development | 105 | SAI    |
|  10 | development | 106 | TOM    |
|  30 | reserach    | 107 | VICKY  |
|  40 | sales       | 108 | SRAVAN |
+-----+-------------+-----+--------+
8 rows in set (0.00 sec)

mysql> select  d.dno ,dname,eid,ename from dept d  JOIN employee e  where e.dno!=d.dno;
+-----+-------------+-----+--------+
| dno | dname       | eid | ename  |
+-----+-------------+-----+--------+
|  50 | travels     | 101 | LIKITH |
|  40 | sales       | 101 | LIKITH |
|  30 | reserach    | 101 | LIKITH |
|  20 | testing     | 101 | LIKITH |
|  50 | travels     | 102 | PAVAN  |
|  40 | sales       | 102 | PAVAN  |
|  30 | reserach    | 102 | PAVAN  |
|  20 | testing     | 102 | PAVAN  |
|  50 | travels     | 103 | VAMSHI |
|  40 | sales       | 103 | VAMSHI |
|  30 | reserach    | 103 | VAMSHI |
|  20 | testing     | 103 | VAMSHI |
|  50 | travels     | 104 | KIRAN  |
|  40 | sales       | 104 | KIRAN  |
|  30 | reserach    | 104 | KIRAN  |
|  20 | testing     | 104 | KIRAN  |
|  50 | travels     | 105 | SAI    |
|  40 | sales       | 105 | SAI    |
|  30 | reserach    | 105 | SAI    |
|  20 | testing     | 105 | SAI    |
|  50 | travels     | 106 | TOM    |
|  40 | sales       | 106 | TOM    |
|  30 | reserach    | 106 | TOM    |
|  20 | testing     | 106 | TOM    |
|  50 | travels     | 107 | VICKY  |
|  40 | sales       | 107 | VICKY  |
|  20 | testing     | 107 | VICKY  |
|  10 | development | 107 | VICKY  |
|  50 | travels     | 108 | SRAVAN |
|  30 | reserach    | 108 | SRAVAN |
|  20 | testing     | 108 | SRAVAN |
|  10 | development | 108 | SRAVAN |
+-----+-------------+-----+--------+
32 rows in set (0.00 sec)

mysql> select  d.dno ,dname,eid,ename from dept d INNER  JOIN employee e  where e.dno=d.dno;;
+-----+-------------+-----+--------+
| dno | dname       | eid | ename  |
+-----+-------------+-----+--------+
|  10 | development | 101 | LIKITH |
|  10 | development | 102 | PAVAN  |
|  10 | development | 103 | VAMSHI |
|  10 | development | 104 | KIRAN  |
|  10 | development | 105 | SAI    |
|  10 | development | 106 | TOM    |
|  30 | reserach    | 107 | VICKY  |
|  40 | sales       | 108 | SRAVAN |
+-----+-------------+-----+--------+
8 rows in set (0.00 sec)

ERROR:
No query specified

mysql> select  d.dno ,dname,eid,ename from dept d left  join  employee e  on(e.dno=d.dno);
+-----+-------------+------+--------+
| dno | dname       | eid  | ename  |
+-----+-------------+------+--------+
|  10 | development |  106 | TOM    |
|  10 | development |  105 | SAI    |
|  10 | development |  104 | KIRAN  |
|  10 | development |  103 | VAMSHI |
|  10 | development |  102 | PAVAN  |
|  10 | development |  101 | LIKITH |
|  20 | testing     | NULL | NULL   |
|  30 | reserach    |  107 | VICKY  |
|  40 | sales       |  108 | SRAVAN |
|  50 | travels     | NULL | NULL   |
+-----+-------------+------+--------+
10 rows in set (0.00 sec)

mysql>
mysql>
mysql> select  d.dno ,dname,eid,ename from dept d right  join  employee e  on(e.dno=d.dno);
+------+-------------+-----+--------+
| dno  | dname       | eid | ename  |
+------+-------------+-----+--------+
|   10 | development | 101 | LIKITH |
|   10 | development | 102 | PAVAN  |
|   10 | development | 103 | VAMSHI |
|   10 | development | 104 | KIRAN  |
|   10 | development | 105 | SAI    |
|   10 | development | 106 | TOM    |
|   30 | reserach    | 107 | VICKY  |
|   40 | sales       | 108 | SRAVAN |
| NULL | NULL        | 109 | JERRY  |
+------+-------------+-----+--------+
9 rows in set (0.00 sec)

mysql>
mysql>
mysql> select  d.dno ,dname,eid,ename from dept d right  join  employee e  on(e.dno=d.dno);
+------+-------------+-----+--------+
| dno  | dname       | eid | ename  |
+------+-------------+-----+--------+
|   10 | development | 101 | LIKITH |
|   10 | development | 102 | PAVAN  |
|   10 | development | 103 | VAMSHI |
|   10 | development | 104 | KIRAN  |
|   10 | development | 105 | SAI    |
|   10 | development | 106 | TOM    |
|   30 | reserach    | 107 | VICKY  |
|   40 | sales       | 108 | SRAVAN |
| NULL | NULL        | 109 | JERRY  |
+------+-------------+-----+--------+
9 rows in set (0.00 sec)

mysql> select  d.dno ,dname,eid,ename from dept d right outer  join  employee e  on(e.dno=d.dno);
+------+-------------+-----+--------+
| dno  | dname       | eid | ename  |
+------+-------------+-----+--------+
|   10 | development | 101 | LIKITH |
|   10 | development | 102 | PAVAN  |
|   10 | development | 103 | VAMSHI |
|   10 | development | 104 | KIRAN  |
|   10 | development | 105 | SAI    |
|   10 | development | 106 | TOM    |
|   30 | reserach    | 107 | VICKY  |
|   40 | sales       | 108 | SRAVAN |
| NULL | NULL        | 109 | JERRY  |
+------+-------------+-----+--------+
9 rows in set (0.00 sec)

mysql>  select  d.dno ,dname,eid,ename from dept d left  join  employee e  on(e.dno=d.dno);
+-----+-------------+------+--------+
| dno | dname       | eid  | ename  |
+-----+-------------+------+--------+
|  10 | development |  106 | TOM    |
|  10 | development |  105 | SAI    |
|  10 | development |  104 | KIRAN  |
|  10 | development |  103 | VAMSHI |
|  10 | development |  102 | PAVAN  |
|  10 | development |  101 | LIKITH |
|  20 | testing     | NULL | NULL   |
|  30 | reserach    |  107 | VICKY  |
|  40 | sales       |  108 | SRAVAN |
|  50 | travels     | NULL | NULL   |
+-----+-------------+------+--------+
10 rows in set (0.00 sec)

mysql>  select  d.dno ,dname,eid,ename from dept d right  join  employee e  on(e.dno=d.dno); UNION
+------+-------------+-----+--------+
| dno  | dname       | eid | ename  |
+------+-------------+-----+--------+
|   10 | development | 101 | LIKITH |
|   10 | development | 102 | PAVAN  |
|   10 | development | 103 | VAMSHI |
|   10 | development | 104 | KIRAN  |
|   10 | development | 105 | SAI    |
|   10 | development | 106 | TOM    |
|   30 | reserach    | 107 | VICKY  |
|   40 | sales       | 108 | SRAVAN |
| NULL | NULL        | 109 | JERRY  |
+------+-------------+-----+--------+
9 rows in set (0.00 sec)

    -> select  d.dno ,dname,eid,ename from dept d right  join  employee e  on(e.dno=d.dno) UNION
    ->  select  d.dno ,dname,eid,ename from dept d left  join  employee e  on(e.dno=d.dno);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'UNION
select  d.dno ,dname,eid,ename from dept d right  join  employee e  on(e.d' at line 1
mysql>  select  d.dno ,dname,eid,ename from dept d right  join  employee e  on(e.dno=d.dno) UNION
    ->   select  d.dno ,dname,eid,ename from dept d left  join  employee e  on(e.dno=d.dno);
+------+-------------+------+--------+
| dno  | dname       | eid  | ename  |
+------+-------------+------+--------+
|   10 | development |  101 | LIKITH |
|   10 | development |  102 | PAVAN  |
|   10 | development |  103 | VAMSHI |
|   10 | development |  104 | KIRAN  |
|   10 | development |  105 | SAI    |
|   10 | development |  106 | TOM    |
|   30 | reserach    |  107 | VICKY  |
|   40 | sales       |  108 | SRAVAN |
| NULL | NULL        |  109 | JERRY  |
|   20 | testing     | NULL | NULL   |
|   50 | travels     | NULL | NULL   |
+------+-------------+------+--------+
11 rows in set (0.00 sec)

mysql> SELECT * from employee e,employee m where e.mid = m.eid;
+-----+--------+----------+---------+-----------+------+------------+------+-----+--------+----------+------+-----------+------+------------+------+
| eid | ename  | salary   | comm    | job       | mid  | doj        | dno  | eid | ename  | salary   | comm | job       | mid  | doj        | dno  |
+-----+--------+----------+---------+-----------+------+------------+------+-----+--------+----------+------+-----------+------+------------+------+
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 | 101 | LIKITH | 90000.00 | NULL | president | NULL | 1998-06-11 |   10 |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 | 102 | PAVAN  | 50000.00 | NULL | manager   |  101 | 1999-06-14 |   10 |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 | 102 | PAVAN  | 50000.00 | NULL | manager   |  101 | 1999-06-14 |   10 |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 | 101 | LIKITH | 90000.00 | NULL | president | NULL | 1998-06-11 |   10 |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 | 105 | SAI    | 43000.00 | NULL | manager   |  101 | 2001-04-05 |   10 |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 | 101 | LIKITH | 90000.00 | NULL | president | NULL | 1998-06-11 |   10 |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 | 101 | LIKITH | 90000.00 | NULL | president | NULL | 1998-06-11 |   10 |
+-----+--------+----------+---------+-----------+------+------------+------+-----+--------+----------+------+-----------+------+------------+------+
7 rows in set (0.00 sec)

mysql> SELECT m.ename,e.ename from employee e,employee m where e.mid = m.eid;
+--------+--------+
| ename  | ename  |
+--------+--------+
| LIKITH | PAVAN  |
| PAVAN  | VAMSHI |
| PAVAN  | KIRAN  |
| LIKITH | SAI    |
| SAI    | TOM    |
| LIKITH | VICKY  |
| LIKITH | SRAVAN |
+--------+--------+
7 rows in set (0.00 sec)

mysql>  SELECT m.ename,e.ename from employee e left join employee m where e.mid = m.eid;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'where e.mid = m.eid' at line 1
mysql> ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'where e.mid = m.eid' at line 1
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'ERROR 1064 (42000): You have an error in your SQL syntax' at line 1
    -> mysql> SELECT m.ename,e.ename from employee e left join employee m on e.mid = m.ei
    -> ^C
mysql> SELECT m.ename,e.ename from employee e left join employee m on e.mid = m.eid;
+--------+--------+
| ename  | ename  |
+--------+--------+
| NULL   | LIKITH |
| LIKITH | PAVAN  |
| PAVAN  | VAMSHI |
| PAVAN  | KIRAN  |
| LIKITH | SAI    |
| SAI    | TOM    |
| LIKITH | VICKY  |
| LIKITH | SRAVAN |
| NULL   | JERRY  |
+--------+--------+
9 rows in set (0.00 sec)

mysql> SELECT IFNULL( m.ename, 'NO BODY'),e.ename from employee e left join employee m on e.mid = m.eid;
+-----------------------------+--------+
| IFNULL( m.ename, 'NO BODY') | ename  |
+-----------------------------+--------+
| NO BODY                     | LIKITH |
| LIKITH                      | PAVAN  |
| PAVAN                       | VAMSHI |
| PAVAN                       | KIRAN  |
| LIKITH                      | SAI    |
| SAI                         | TOM    |
| LIKITH                      | VICKY  |
| LIKITH                      | SRAVAN |
| NO BODY                     | JERRY  |
+-----------------------------+--------+
9 rows in set (0.00 sec)

mysql> select * from employee;
+-----+--------+----------+---------+-----------+------+------------+------+
| eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+--------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
+-----+--------+----------+---------+-----------+------+------------+------+
9 rows in set (0.00 sec)

mysql> select dno from dept where location='chennai';
ERROR 1054 (42S22): Unknown column 'location' in 'where clause'
mysql> select dno from dept where location='hyderabad';
ERROR 1054 (42S22): Unknown column 'location' in 'where clause'
mysql> select * from  employee where dno=(select dno from dept where location='chennai');
ERROR 1054 (42S22): Unknown column 'location' in 'where clause'
mysql>  select eid,ename,salary,job,doj from employee e, dept d where e.dno=d.dno and location =banglore';
    '>  select eid,ename,salary,job,doj from employee e, dept d where e.dno=d.dno and location ='banglore';
    '> select * from dept  where dno IN (select dno from employee where job='manager');
    '> ^C
mysql> select * from dept  where dno IN (select dno from employee where job='manager');
+-----+-------------+-----------+
| dno | dname       | loacation |
+-----+-------------+-----------+
|  10 | development | banglore  |
+-----+-------------+-----------+
1 row in set (0.00 sec)

mysql>  select eid,ename,salary,job,doj from employee e, dept d where e.dno=d.dno and location ='banglore';
ERROR 1054 (42S22): Unknown column 'location' in 'where clause'
mysql> SELECT * from employee where salary >(select salary from employee where ename='tom');
+-----+--------+----------+---------+-----------+------+------------+------+
| eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+--------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
+-----+--------+----------+---------+-----------+------+------------+------+
6 rows in set (0.00 sec)

mysql> select * from employee;
+-----+--------+----------+---------+-----------+------+------------+------+
| eid | ename  | salary   | comm    | job       | mid  | doj        | dno  |
+-----+--------+----------+---------+-----------+------+------------+------+
| 101 | LIKITH | 90000.00 |    NULL | president | NULL | 1998-06-11 |   10 |
| 102 | PAVAN  | 50000.00 |    NULL | manager   |  101 | 1999-06-14 |   10 |
| 103 | VAMSHI | 35000.00 |    NULL | developer |  102 | 2000-02-11 |   10 |
| 104 | KIRAN  | 32000.00 |    NULL | developer |  102 | 2001-04-05 |   10 |
| 105 | SAI    | 43000.00 |    NULL | manager   |  101 | 2001-04-05 |   10 |
| 106 | TOM    | 25000.00 | 2000.00 | tester    |  105 | 1998-04-05 |   10 |
| 107 | VICKY  | 65000.00 | 5000.00 | analyst   |  101 | 1992-04-11 |   30 |
| 108 | SRAVAN | 15000.00 | 3000.00 | salesman  |  101 | 2021-12-29 |   40 |
| 109 | JERRY  |  5000.00 |  300.00 | clerk     | NULL | 2021-12-29 | NULL |
+-----+--------+----------+---------+-----------+------+------------+------+
9 rows in set (0.00 sec)

mysql> select job,sum (salary) from employee group by job;
ERROR 1630 (42000): FUNCTION ems.sum does not exist. Check the 'Function Name Parsing and Resolution' section in the Reference Manual
mysql> select job,sum(salary) from employee group by job;
+-----------+-------------+
| job       | sum(salary) |
+-----------+-------------+
| president |    90000.00 |
| manager   |    93000.00 |
| developer |    67000.00 |
| tester    |    25000.00 |
| analyst   |    65000.00 |
| salesman  |    15000.00 |
| clerk     |     5000.00 |
+-----------+-------------+
7 rows in set (0.00 sec)

mysql> select job,sum(salary) from employee where sum(salary)>50000 group by job;
ERROR 1111 (HY000): Invalid use of group function
mysql>  select job,sum(salary) from employee where  job!='salesman' group by job;
+-----------+-------------+
| job       | sum(salary) |
+-----------+-------------+
| president |    90000.00 |
| manager   |    93000.00 |
| developer |    67000.00 |
| tester    |    25000.00 |
| analyst   |    65000.00 |
| clerk     |     5000.00 |
+-----------+-------------+
6 rows in set (0.00 sec)

mysql> select job,sum(salary) from employee where  job!='salesman' group by job having sum(salary)>50000;
+-----------+-------------+
| job       | sum(salary) |
+-----------+-------------+
| president |    90000.00 |
| manager   |    93000.00 |
| developer |    67000.00 |
| analyst   |    65000.00 |
+-----------+-------------+
4 rows in set (0.00 sec)

mysql> select job,sum(salary) from employee where  job!='salesman' group by job having sum(salary)>50000 order by sum(salary);
+-----------+-------------+
| job       | sum(salary) |
+-----------+-------------+
| analyst   |    65000.00 |
| developer |    67000.00 |
| president |    90000.00 |
| manager   |    93000.00 |
+-----------+-------------+
4 rows in set (0.00 sec)

mysql> select job,sum(salary) from employee where  job!='salesman' group by job having sum(salary)>50000 order by ename;
+-----------+-------------+
| job       | sum(salary) |
+-----------+-------------+
| president |    90000.00 |
| manager   |    93000.00 |
| developer |    67000.00 |
| analyst   |    65000.00 |
+-----------+-------------+
4 rows in set (0.00 sec)

mysql> select job,sum(salary) from employee ;
+-----------+-------------+
| job       | sum(salary) |
+-----------+-------------+
| president |   360000.00 |
+-----------+-------------+
1 row in set (0.00 sec)

mysql> select job,count(job), sum(salary) from employee group by job;
+-----------+------------+-------------+
| job       | count(job) | sum(salary) |
+-----------+------------+-------------+
| president |          1 |    90000.00 |
| manager   |          2 |    93000.00 |
| developer |          2 |    67000.00 |
| tester    |          1 |    25000.00 |
| analyst   |          1 |    65000.00 |
| salesman  |          1 |    15000.00 |
| clerk     |          1 |     5000.00 |
+-----------+------------+-------------+
7 rows in set (0.00 sec)