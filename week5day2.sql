Enter password: **************
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 15
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database Restarunt
    -> ;
Query OK, 1 row affected (0.02 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| ems                |
| hcl                |
| information_schema |
| moviedatabase      |
| movies             |
| moviesdatabase     |
| mysql              |
| performance_schema |
| restarunt          |
| sakila             |
| sys                |
| week1assignment1   |
| world              |
+--------------------+
13 rows in set (0.01 sec)

mysql> use Restarunt;
Database changed
mysql> show tables;
Empty set (0.01 sec)

mysql> create table Restarunt(    Id int (primary key)
    ->
    -> ,
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'primary key)

,' at line 1
mysql> create table Restarunt(Id INTEGER (primary key), Name varchar(30));
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'primary key), Name varchar(30))' at line 1
mysql> create table Restarunt(Id INTEGER primary key, Name varchar(30));
Query OK, 0 rows affected (0.12 sec)

mysql> create table Dishes(DishId INTEGER,DishName VARCHAR(30),Price FLOAT,RestaurantId int → it references the Id of Restaurant table
    -> ;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '→ it references the Id of Restaurant table' at line 1
mysql> create table Dishes(DishId INTEGER,DishName VARCHAR(30),Price FLOAT,
    -> restaurant_id integer REFERENCES restaurant(id)
    -> );
Query OK, 0 rows affected (0.07 sec)

mysql> insert into Restarunt(1,Punjabi Rasoi);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '1,Punjabi Rasoi)' at line 1
mysql> insert into Restarunt(1,'Punjabi Rasoi');
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '1,'Punjabi Rasoi')' at line 1
mysql> desc Restarunt;
+-------+-------------+------+-----+---------+-------+
| Field | Type        | Null | Key | Default | Extra |
+-------+-------------+------+-----+---------+-------+
| Id    | int         | NO   | PRI | NULL    |       |
| Name  | varchar(30) | YES  |     | NULL    |       |
+-------+-------------+------+-----+---------+-------+
2 rows in set (0.02 sec)

mysql> INSERT INTO Restarunt Values(1,'Punjabi Rasoi');
Query OK, 1 row affected (0.02 sec)

mysql> INSERT INTO Restarunt Values(2,'Udupi Grand');
Query OK, 1 row affected (0.01 sec)

mysql> INSERT INTO Restarunt Values(3.'BBQ nation');
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''BBQ nation')' at line 1
mysql> INSERT INTO Restarunt Values(3,'BBQ nation');
Query OK, 1 row affected (0.01 sec)

mysql> select*from Restarunt;
+----+---------------+
| Id | Name          |
+----+---------------+
|  1 | Punjabi Rasoi |
|  2 | Udupi Grand   |
|  3 | BBQ nation    |
+----+---------------+
3 rows in set (0.00 sec)

mysql> INSERT INTO Dishes Value (1,'Dal Makhni',120,1);
Query OK, 1 row affected (0.02 sec)

mysql> INSERT INTO Dishes Value (2, 'Sarso Saag',100,1),
    -> (3,'Tandoori Roti',12 ,1),
    -> (4,'Masala Dosa',40, 2),
    -> (5,'Rava Idly',50,2),
    -> (6 ,'Vada',30,2),
    -> (7,' pizza',150,3),
    -> (8,'burger',80,3),
    -> (9,'Momos',50,3);
Query OK, 8 rows affected (0.01 sec)
Records: 8  Duplicates: 0  Warnings: 0

mysql> select *from Dishes;
+--------+---------------+-------+---------------+
| DishId | DishName      | Price | restaurant_id |
+--------+---------------+-------+---------------+
|      1 | Dal Makhni    |   120 |             1 |
|      2 | Sarso Saag    |   100 |             1 |
|      3 | Tandoori Roti |    12 |             1 |
|      4 | Masala Dosa   |    40 |             2 |
|      5 | Rava Idly     |    50 |             2 |
|      6 | Vada          |    30 |             2 |
|      7 |  pizza        |   150 |             3 |
|      8 | burger        |    80 |             3 |
|      9 | Momos         |    50 |             3 |
+--------+---------------+-------+---------------+
9 rows in set (0.00 sec)

mysql> select *from Dishes d, Restarunt as r where  r.id= d.id;
ERROR 1054 (42S22): Unknown column 'd.id' in 'where clause'
mysql> select *from Dishes as  d, Restarunt as r where  r.id= d.id;
ERROR 1054 (42S22): Unknown column 'd.id' in 'where clause'
mysql> select *from Dishes as  D, Restarunt as R where  R.id= D.id;
ERROR 1054 (42S22): Unknown column 'D.id' in 'where clause'
mysql> select *from Dishes as  D, Restarunt as R where  R.id= Dish.id;
ERROR 1054 (42S22): Unknown column 'Dish.id' in 'where clause'
mysql> select *from Dishes as  D, Restarunt as R where  restarunt.id= Dish.id;
ERROR 1054 (42S22): Unknown column 'restarunt.id' in 'where clause'
mysql> SELECT * from Dishes, Restarunt where  DishId= RestaruntId;
ERROR 1054 (42S22): Unknown column 'RestaruntId' in 'where clause'
mysql> SELECT * from Dishes, Restarunt where  DishId= Restarunt_Id;
ERROR 1054 (42S22): Unknown column 'Restarunt_Id' in 'where clause'
mysql> SELECT * from Dishes, Restarunt where  DishId= Id;
+--------+---------------+-------+---------------+----+---------------+
| DishId | DishName      | Price | restaurant_id | Id | Name          |
+--------+---------------+-------+---------------+----+---------------+
|      1 | Dal Makhni    |   120 |             1 |  1 | Punjabi Rasoi |
|      2 | Sarso Saag    |   100 |             1 |  2 | Udupi Grand   |
|      3 | Tandoori Roti |    12 |             1 |  3 | BBQ nation    |
+--------+---------------+-------+---------------+----+---------------+
3 rows in set (0.00 sec)

mysql> select *from Dishes as  D, Restarunt as R where  R.id= D.id;
ERROR 1054 (42S22): Unknown column 'D.id' in 'where clause'
mysql> select *from Dishes as  D, Restarunt as R where  R.id= DishId;
+--------+---------------+-------+---------------+----+---------------+
| DishId | DishName      | Price | restaurant_id | Id | Name          |
+--------+---------------+-------+---------------+----+---------------+
|      1 | Dal Makhni    |   120 |             1 |  1 | Punjabi Rasoi |
|      2 | Sarso Saag    |   100 |             1 |  2 | Udupi Grand   |
|      3 | Tandoori Roti |    12 |             1 |  3 | BBQ nation    |
+--------+---------------+-------+---------------+----+---------------+
3 rows in set (0.00 sec)

mysql> SELECT id,name,DishId,DIshName from Dish D INNER JOIN Restarunt as R where  R.id= D.id;
ERROR 1146 (42S02): Table 'restarunt.dish' doesn't exist

mysql> SELECT Id,Name,DishId,DIshName from Dish as D INNER JOIN Restarunt as R where  R.id= DishId;
ERROR 1146 (42S02): Table 'restarunt.dish' doesn't exist

mysql> SELECT DishId,DishName,Id,Name from Dish as D INNER JOIN Restarunt as R where  id= DishId;
ERROR 1146 (42S02): Table 'restarunt.dish' doesn't exist
mysql> select *from Restarunt;
+----+---------------+
| Id | Name          |
+----+---------------+
|  1 | Punjabi Rasoi |
|  2 | Udupi Grand   |
|  3 | BBQ nation    |
+----+---------------+
3 rows in set (0.00 sec)

mysql> select*from Dishes;
+--------+---------------+-------+---------------+
| DishId | DishName      | Price | restaurant_id |
+--------+---------------+-------+---------------+
|      1 | Dal Makhni    |   120 |             1 |
|      2 | Sarso Saag    |   100 |             1 |
|      3 | Tandoori Roti |    12 |             1 |
|      4 | Masala Dosa   |    40 |             2 |
|      5 | Rava Idly     |    50 |             2 |
|      6 | Vada          |    30 |             2 |
|      7 |  pizza        |   150 |             3 |
|      8 | burger        |    80 |             3 |
|      9 | Momos         |    50 |             3 |
+--------+---------------+-------+---------------+
9 rows in set (0.00 sec)

mysql> SELECT DishId,DishName,Id,Name from Dish as D INNER JOIN Restaurunt as R where  id= DishId;
ERROR 1146 (42S02): Table 'restarunt.dish' doesn't exist
mysql> SELECT DishId,DishName,Id,Name from Dish as D INNER JOIN Restaurunt as R where  id= DishId.Id;
ERROR 1146 (42S02): Table 'restarunt.dish' doesn't exist
mysql>