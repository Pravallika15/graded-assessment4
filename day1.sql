
mysql> desc employee
    -> ;
ERROR 1046 (3D000): No database selected
mysql> create database hcl;
ERROR 1007 (HY000): Can't create database 'hcl'; database exists
mysql> show database;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'database' at line 1
mysql> show databases
    -> ;
+--------------------+
| Database           |
+--------------------+
| hcl                |
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| world              |
+--------------------+
7 rows in set (0.00 sec)

mysql> use hcl;
Database changed
mysql> CREATE TABLE  employee(eid INTEGER(3),ename  VARCHAR(20),salary DECIMAL,doj DATE);
ERROR 1050 (42S01): Table 'employee' already exists
mysql> show tables;
+---------------+
| Tables_in_hcl |
+---------------+
| employee      |
+---------------+
1 row in set (0.00 sec)

mysql> describe employee;
+--------+---------------+------+-----+---------+-------+
| Field  | Type          | Null | Key | Default | Extra |
+--------+---------------+------+-----+---------+-------+
| eid    | int           | YES  |     | NULL    |       |
| ename  | varchar(30)   | YES  |     | NULL    |       |
| salary | decimal(10,0) | YES  |     | NULL    |       |
| doj    | date          | YES  |     | NULL    |       |
| city   | varchar(10)   | YES  |     | NULL    |       |
+--------+---------------+------+-----+---------+-------+
5 rows in set (0.00 sec)

mysql> alter table  employee  add column  city varchar(20);
ERROR 1060 (42S21): Duplicate column name 'city'
mysql> ALTER TABLE employee modify(ename  varchar(30));
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '(ename  varchar(30))' at line 1
mysql>
mysql> ALTER TABLE employee modify ename  varchar(30);
Query OK, 0 rows affected (0.13 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> desc employee;
+--------+---------------+------+-----+---------+-------+
| Field  | Type          | Null | Key | Default | Extra |
+--------+---------------+------+-----+---------+-------+
| eid    | int           | YES  |     | NULL    |       |
| ename  | varchar(30)   | YES  |     | NULL    |       |
| salary | decimal(10,0) | YES  |     | NULL    |       |
| doj    | date          | YES  |     | NULL    |       |
| city   | varchar(10)   | YES  |     | NULL    |       |
+--------+---------------+------+-----+---------+-------+
5 rows in set (0.01 sec)

mysql> select*from employee;
Empty set (0.00 sec)

mysql> insert into employee  values(101,'pravallika',40000,'2021-12-28','banglore');
Query OK, 1 row affected (0.01 sec)

mysql> select *from employee;
+------+------------+--------+------------+----------+
| eid  | ename      | salary | doj        | city     |
+------+------------+--------+------------+----------+
|  101 | pravallika |  40000 | 2021-12-28 | banglore |
+------+------------+--------+------------+----------+
1 row in set (0.00 sec)

mysql> show tables;
+---------------+
| Tables_in_hcl |
+---------------+
| employee      |
+---------------+
1 row in set (0.10 sec)

mysql> insert into employee  values(102,'sravani',50000,'2006-08-15','hyderabad');
Query OK, 1 row affected (0.11 sec)

mysql> insert into employee  values(103,'usha',30000,'2021-11-21','Anantapur');
Query OK, 1 row affected (0.12 sec)

mysql> ^C
mysql> insert into employee  values(104,'likith',25000,'2021-12-24,'kurnool');
    '>
    '> select*from employee;
    '>
    '>
    '> insert into employee  values(104,'likith',25000,'2021-12-24,'kurnool');
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'kurnool');

select*from employee;


insert into employee  values(104,'likith',25' at line 1
mysql> insert into employee  values(104,'nikhil',30000,'2021-12-27', 'pune') , (105, 'likith',25000,'2021-12-28' ,'chennai') ;
Query OK, 2 rows affected (0.01 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> select *from employee;
+------+------------+--------+------------+-----------+
| eid  | ename      | salary | doj        | city      |
+------+------------+--------+------------+-----------+
|  101 | pravallika |  40000 | 2021-12-28 | banglore  |
|  102 | sravani    |  50000 | 2006-08-15 | hyderabad |
|  103 | usha       |  30000 | 2021-11-21 | Anantapur |
|  104 | nikhil     |  30000 | 2021-12-27 | pune      |
|  105 | likith     |  25000 | 2021-12-28 | chennai   |
+------+------------+--------+------------+-----------+
5 rows in set (0.00 sec)

mysql> select eid , ename , salary  from employee;
+------+------------+--------+
| eid  | ename      | salary |
+------+------------+--------+
|  101 | pravallika |  40000 |
|  102 | sravani    |  50000 |
|  103 | usha       |  30000 |
|  104 | nikhil     |  30000 |
|  105 | likith     |  25000 |
+------+------------+--------+
5 rows in set (0.00 sec)

mysql> select ename , salary  from employee;
+------------+--------+
| ename      | salary |
+------------+--------+
| pravallika |  40000 |
| sravani    |  50000 |
| usha       |  30000 |
| nikhil     |  30000 |
| likith     |  25000 |
+------------+--------+
5 rows in set (0.00 sec)

mysql> select ename as emp_name , salary as  emp_salary from employee;
+------------+------------+
| emp_name   | emp_salary |
+------------+------------+
| pravallika |      40000 |
| sravani    |      50000 |
| usha       |      30000 |
| nikhil     |      30000 |
| likith     |      25000 |
+------------+------------+
5 rows in set (0.00 sec)

mysql>  select eid , ename , salary  from employee;
+------+------------+--------+
| eid  | ename      | salary |
+------+------------+--------+
|  101 | pravallika |  40000 |
|  102 | sravani    |  50000 |
|  103 | usha       |  30000 |
|  104 | nikhil     |  30000 |
|  105 | likith     |  25000 |
+------+------------+--------+
5 rows in set (0.00 sec)

mysql> select *  from  employee  where eid = 101;
+------+------------+--------+------------+----------+
| eid  | ename      | salary | doj        | city     |
+------+------------+--------+------------+----------+
|  101 | pravallika |  40000 | 2021-12-28 | banglore |
+------+------------+--------+------------+----------+
1 row in set (0.00 sec)

mysql> select *  from  employee  where eid != 101;
+------+---------+--------+------------+-----------+
| eid  | ename   | salary | doj        | city      |
+------+---------+--------+------------+-----------+
|  102 | sravani |  50000 | 2006-08-15 | hyderabad |
|  103 | usha    |  30000 | 2021-11-21 | Anantapur |
|  104 | nikhil  |  30000 | 2021-12-27 | pune      |
|  105 | likith  |  25000 | 2021-12-28 | chennai   |
+------+---------+--------+------------+-----------+
4 rows in set (0.00 sec)

mysql> select * from employee where  salary > 30000;
+------+------------+--------+------------+-----------+
| eid  | ename      | salary | doj        | city      |
+------+------------+--------+------------+-----------+
|  101 | pravallika |  40000 | 2021-12-28 | banglore  |
|  102 | sravani    |  50000 | 2006-08-15 | hyderabad |
+------+------------+--------+------------+-----------+
2 rows in set (0.00 sec)

mysql> select * from employee where  salary >= 30000;
+------+------------+--------+------------+-----------+
| eid  | ename      | salary | doj        | city      |
+------+------------+--------+------------+-----------+
|  101 | pravallika |  40000 | 2021-12-28 | banglore  |
|  102 | sravani    |  50000 | 2006-08-15 | hyderabad |
|  103 | usha       |  30000 | 2021-11-21 | Anantapur |
|  104 | nikhil     |  30000 | 2021-12-27 | pune      |
+------+------------+--------+------------+-----------+
4 rows in set (0.00 sec)

mysql> select * from employee where  salary <= 30000;
+------+--------+--------+------------+-----------+
| eid  | ename  | salary | doj        | city      |
+------+--------+--------+------------+-----------+
|  103 | usha   |  30000 | 2021-11-21 | Anantapur |
|  104 | nikhil |  30000 | 2021-12-27 | pune      |
|  105 | likith |  25000 | 2021-12-28 | chennai   |
+------+--------+--------+------------+-----------+
3 rows in set (0.00 sec)

mysql>  select   IFNULL(salary,0) + 100 from employee;
+------------------------+
| IFNULL(salary,0) + 100 |
+------------------------+
|                  40100 |
|                  50100 |
|                  30100 |
|                  30100 |
|                  25100 |
+------------------------+
5 rows in set (0.00 sec)

mysql> select doj from employee;
+------------+
| doj        |
+------------+
| 2021-12-28 |
| 2006-08-15 |
| 2021-11-21 |
| 2021-12-27 |
| 2021-12-28 |
+------------+
5 rows in set (0.00 sec)

mysql> select IFNULL(doj,current_date) from employee;
+--------------------------+
| IFNULL(doj,current_date) |
+--------------------------+
| 2021-12-28               |
| 2006-08-15               |
| 2021-11-21               |
| 2021-12-27               |
| 2021-12-28               |
+--------------------------+
5 rows in set (0.00 sec)

mysql> select city from employee;
+-----------+
| city      |
+-----------+
| banglore  |
| hyderabad |
| Anantapur |
| pune      |
| chennai   |
+-----------+
5 rows in set (0.00 sec)

mysql> insert into employee values(106,'hemanth',15000,'2021-12-15;','null');
Query OK, 1 row affected (0.12 sec)

mysql> select *from employee;
+------+------------+--------+------------+-----------+
| eid  | ename      | salary | doj        | city      |
+------+------------+--------+------------+-----------+
|  101 | pravallika |  40000 | 2021-12-28 | banglore  |
|  102 | sravani    |  50000 | 2006-08-15 | hyderabad |
|  103 | usha       |  30000 | 2021-11-21 | Anantapur |
|  104 | nikhil     |  30000 | 2021-12-27 | pune      |
|  105 | likith     |  25000 | 2021-12-28 | chennai   |
|  106 | hemanth    |  15000 | 2021-12-15 | null      |
+------+------------+--------+------------+-----------+
6 rows in set (0.09 sec)

mysql> select sum(salary)  from employee;
+-------------+
| sum(salary) |
+-------------+
|      190000 |
+-------------+
1 row in set (0.00 sec)

mysql> select  avg(salary) from employee;
+-------------+
| avg(salary) |
+-------------+
|  31666.6667 |
+-------------+
1 row in set (0.00 sec)

mysql> select sum(salary)  from employee  group by city;
+-------------+
| sum(salary) |
+-------------+
|       40000 |
|       50000 |
|       30000 |
|       30000 |
|       25000 |
|       15000 |
+-------------+
6 rows in set (0.11 sec)

mysql> select sum(eid)  from employee  group by city;
+----------+
| sum(eid) |
+----------+
|      101 |
|      102 |
|      103 |
|      104 |
|      105 |
|      106 |
+----------+
6 rows in set (0.00 sec)

mysql> select city, sum(eid)  from employee  group by city;
+-----------+----------+
| city      | sum(eid) |
+-----------+----------+
| banglore  |      101 |
| hyderabad |      102 |
| Anantapur |      103 |
| pune      |      104 |
| chennai   |      105 |
| null      |      106 |
+-----------+----------+
6 rows in set (0.00 sec)

mysql> select city,sum(eid)  from employee  group by city;
+-----------+----------+
| city      | sum(eid) |
+-----------+----------+
| banglore  |      101 |
| hyderabad |      102 |
| Anantapur |      103 |
| pune      |      104 |
| chennai   |      105 |
| null      |      106 |
+-----------+----------+
6 rows in set (0.00 sec)

mysql> select city,sum(salary)  from employee  group by city;
+-----------+-------------+
| city      | sum(salary) |
+-----------+-------------+
| banglore  |       40000 |
| hyderabad |       50000 |
| Anantapur |       30000 |
| pune      |       30000 |
| chennai   |       25000 |
| null      |       15000 |
+-----------+-------------+
6 rows in set (0.00 sec)

mysql> select sum(eid)  from employee  group by city;
+----------+
| sum(eid) |
+----------+
|      101 |
|      102 |
|      103 |
|      104 |
|      105 |
|      106 |
+----------+
6 rows in set (0.00 sec)

mysql> select eid,sum(eid)  from employee  group by city;
+------+----------+
| eid  | sum(eid) |
+------+----------+
|  101 |      101 |
|  102 |      102 |
|  103 |      103 |
|  104 |      104 |
|  105 |      105 |
|  106 |      106 |
+------+----------+
6 rows in set (0.00 sec)

mysql> select ename,sum(eid)  from employee  group by city;
+------------+----------+
| ename      | sum(eid) |
+------------+----------+
| pravallika |      101 |
| sravani    |      102 |
| usha       |      103 |
| nikhil     |      104 |
| likith     |      105 |
| hemanth    |      106 |
+------------+----------+
6 rows in set (0.00 sec)

mysql> select ename,sum(eid)  from employee  group by city,ename;
+------------+----------+
| ename      | sum(eid) |
+------------+----------+
| pravallika |      101 |
| sravani    |      102 |
| usha       |      103 |
| nikhil     |      104 |
| likith     |      105 |
| hemanth    |      106 |
+------------+----------+
6 rows in set (0.00 sec)